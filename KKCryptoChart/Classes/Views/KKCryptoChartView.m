//
//  KKCryptoChart.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/1.
//

#import "KKCryptoChartView.h"
#import "KKCryptoChartModel.h"
#import "DateUtil.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChartDataManager.h"
#import "KKCryptoChartGlobalConfig.h"
#import "KKIndicatorSelector.h"
#import "KKCryptoChartTabBar.h"
#import "KKCryptoMoreSelector.h"
#import "TDIndicatorValueFormatter.h"
#import "KKIndicatorDescription.h"
#import "KKCryptoChart-Swift.h"
#import "BundleUtil.h"
#import "KKWebSocket.h"
#import "KKCryptoChartAxisInfoLayer.h"

#define CANDLE_CHART_CANDLE_COUNT 45
#define CHART_MIN_VIEW_RANGE CANDLE_CHART_CANDLE_COUNT/3
#define CHART_MAX_VIEW_RANGE CANDLE_CHART_CANDLE_COUNT*2
#define XAXIS_LABEL_COUNT 4
#define CHART_TABBAR_HEIGHT 28

static const NSInteger KKCryptoChartTabBarMore = KKCryptoChartTimeTypeOneDay + 1001;
static const NSInteger KKCryptoChartTabBarIndicator = KKCryptoChartTimeTypeOneDay + 1002;
static const CGFloat KKCryptoChartXAxisOffset = 0.5f;
static const CGFloat KKCryptoChartXAxisRightOffset = 8.5f;

@interface KKCryptoChartView() <UIGestureRecognizerDelegate, ChartViewDelegate, KKIndicatorSelectorDelegate, KKCryptoChartTabBarDelegate, KKCryptoMoreSelectorDelegate, KKWebSocketDelegate>

@property (nonatomic, strong) CombinedChartView *mainChartView;
@property (nonatomic, strong) CombinedChartView *volumeChartView;
@property (nonatomic, strong) CombinedChartView *indicatorChartView;//MACD
@property (nonatomic, strong) CombinedChartView *kdjChartView;//KDJ
@property (nonatomic, strong) CombinedChartView *rsiChartView;//RSI
@property (nonatomic, strong) CombinedChartView *wrChartView;//WR
@property (nonatomic, strong) KKCryptoChartTabBar *chartTabBar;
@property (nonatomic, strong) KKCryptoMoreSelector *moreSelector;
@property (nonatomic, strong) KKIndicatorSelector *indicatorSelector;
@property (nonatomic, strong) KKIndicatorDescription *mainIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *volumeIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *sideIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *kdjIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *rsiIndicatorDesc;
@property (nonatomic, strong) KKIndicatorDescription *wrIndicatorDesc;
@property (nonatomic, strong) ChartYAxis *mainChartViewRightAxis;

@property (strong, nonatomic) UIActivityIndicatorView *loadingView;
@property (nonatomic, assign) CGFloat chartViewHeight;

@property (strong, nonatomic) CombinedChartData *mainData;
@property (strong, nonatomic) CombinedChartData *volumeData;
@property (strong, nonatomic) CombinedChartData *indicatorData;
@property (strong, nonatomic) CombinedChartData *kdjIndicatorData;
@property (strong, nonatomic) CombinedChartData *rsiIndicatorData;
@property (strong, nonatomic) CombinedChartData *wrIndicatorData;
@property (nonatomic, strong) NSArray<KKCryptoChartModel *> *chartModels;
@property (nonatomic, strong) KKCryptoChartDataManager *dataManager;
@property (nonatomic, assign) KKIndicator currentMainIndType;

@property (nonatomic, strong) NSArray<NSNumber *> *tabBarTypes;
@property (nonatomic, strong) NSArray<NSString *> *tabBarTitles;
@property (nonatomic, strong) NSArray<NSNumber *> *moreSelectorTypes;
@property (nonatomic, strong) NSArray<NSString *> *moreSelectorTitles;

@property (nonatomic, strong) CAShapeLayer *horizontalLineLayer;
@property (nonatomic, strong) CAShapeLayer *verticalLineLayer;
@property (nonatomic, strong) CAShapeLayer *circleLayer;
@property (nonatomic, strong) CAShapeLayer *pointLayer;
@property (nonatomic, strong) KKCryptoChartInfoLayer *infoLayer;
// 点击点对应的交易量文本
@property (nonatomic, strong) KKCryptoChartAxisInfoLayer *volumeLayer;
// 点击点对应的价格文本
@property (nonatomic, strong) KKCryptoChartAxisInfoLayer *priceLayer;
// 当前价格线
@property (nonatomic, strong) ChartLimitLine *priceLine;
// 当前价格按钮
@property (nonatomic, strong) UIButton *priceButton;

@property (nonatomic, strong) KKWebSocket *webSocket;
@property (nonatomic, copy) NSString *fromSymbol;
@property (nonatomic, strong) KKCryptoChartModel *currentChartModel;//当前点击选中的一组数据
@property (nonatomic, strong) ChartHighlight *highlight;//当前的十字交叉线对象
@property (nonatomic, assign) CGFloat highLightY;//
@property (nonatomic, assign) CGFloat visibleHighestPrice;//Y轴刻度最大值
@property (nonatomic, assign) CGFloat visibleLowestPrice;//Y轴刻度最小值
@property (nonatomic, assign) BOOL isLongPress;//是否在长按中
@property (nonatomic, strong) NSMutableArray *showChartViewArray;//副图中当前展示在页面内的k线
@property (nonatomic, strong) NSMutableArray *showIndicatorChartArray;//副图数组
@property (nonatomic, assign) NSInteger chartRightEmpty;
@end

@implementation KKCryptoChartView

#pragma mark - init UI

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initConstantData];
        [self initUI:self.frame];
        [self setUpUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initConstantData];
        [self initUI:frame];
        [self setUpUI];
    }
    return self;
}

- (void)initUI:(CGRect)frame {
    [self initTabBarUI:frame];
    [self initChartUI:frame];
    [self initLoadingUI:frame];
}

// init tabBar ui
- (void)initTabBarUI:(CGRect)frame {
    self.chartTabBar = [[KKCryptoChartTabBar alloc] init];
    self.chartTabBar.delegate = self;
    [self addSubview:self.chartTabBar];
}

- (void)initChartUI:(CGRect)frame {
    self.mainChartView = [[CombinedChartView alloc] init];
    [self addSubview:self.mainChartView];
    
    self.volumeChartView = [[CombinedChartView alloc] init];
    [self addSubview:self.volumeChartView];
    
}

- (void)initLoadingUI:(CGRect)frame {
    if (@available(iOS 13.0, *)) {
        self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    } else {
        self.loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    [self addSubview:self.loadingView];
}

#pragma mark - dealloc

- (void)dealloc {
    if (self.webSocket) {
        [self.webSocket close];
        self.webSocket = nil;
    }
}

#pragma mark - layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self setViewsFrame:self.frame];
}

- (void)setViewsFrame:(CGRect)frame {
    // tab bar
    [self.chartTabBar setFrame:CGRectMake(0, 0, frame.size.width, CHART_TABBAR_HEIGHT)];

    // main chart
    self.chartViewHeight = frame.size.height - CGRectGetHeight(self.chartTabBar.frame) - 15;
    
    CGFloat indicatorHight = 0;
    switch (self.showChartViewArray.count) {
        case 0: {
            CGFloat mainChartH = self.chartViewHeight * 0.8; //正常情况按80:20分割
            [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
            [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * 0.20 + 15)];
        }
            break;
        case 1: {
            CGFloat mainChartH = self.chartViewHeight * 0.65; //正常情况按65:20:15分割
            [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
            [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * 0.20)];
            indicatorHight = self.chartViewHeight * 0.15;
        }
            break;
        case 2:{
            CGFloat mainChartH = self.chartViewHeight * 0.60; //正常情况按60:16:12:12分割
            [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
            [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * 0.16)];
            indicatorHight = self.chartViewHeight * 0.12;
        }
            break;
        case 3:{
            CGFloat mainChartH = self.chartViewHeight * 0.55; //正常情况按55:12:11:11:11分割
            [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
            [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * 0.12)];
            indicatorHight = self.chartViewHeight * 0.11;
        }
            break;
        case 4:{
            CGFloat mainChartH = self.chartViewHeight * 0.50; //正常情况按50:10:10:10:10:10分割
            [self.mainChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame), frame.size.width, mainChartH)];
            [self.volumeChartView setFrame:CGRectMake(0, CGRectGetMaxY(self.mainChartView.frame), frame.size.width, self.chartViewHeight * 0.10)];
            indicatorHight = self.chartViewHeight * 0.10;
        }
            break;
        default:
            break;
    }
    
    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            [combineView setFrame:CGRectMake(0, CGRectGetMaxY(self.volumeChartView.frame) + i * indicatorHight, frame.size.width, indicatorHight)];
            if (i == self.showChartViewArray.count - 1) {
                [combineView setFrame:CGRectMake(0, CGRectGetMaxY(self.volumeChartView.frame) + i * indicatorHight, frame.size.width, indicatorHight + 15)];
            }
        }
    }
    [self.loadingView setFrame:CGRectMake((CGRectGetWidth(frame) / 2) - self.loadingView.frame.size.width, (CGRectGetHeight(frame) / 2) + (CGRectGetHeight(self.chartTabBar.frame) / 2) - self.loadingView.frame.size.height, self.loadingView.frame.size.width, self.loadingView.frame.size.height)];
}

- (void)changeInfoLayerFrame:(CGFloat)x {
    //取消动画
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    CGFloat paddingRight = 60;
    if (x > (self.frame.size.width - paddingRight)/2) {
        self.infoLayer.frame = CGRectMake(0, 50, self.infoLayer.frame.size.width, self.infoLayer.frame.size.height);
    } else {
        self.infoLayer.frame = CGRectMake(self.mainChartView.frame.size.width - paddingRight - self.infoLayer.frame.size.width, 50, self.infoLayer.frame.size.width, self.infoLayer.frame.size.height);
    }
    [CATransaction commit];
}

#pragma mark - set up chart view

- (void)setUpUI {
    [self setUpMainChartView];
    [self setUpVolumeChartView];
    [self addGesture];
}

- (void)setUpMainChartView {
    // delegate
    self.mainChartView.delegate = self;
    
    // hightlight
    self.mainChartView.highlightPerTapEnabled = NO;
    self.mainChartView.highlightPerDragEnabled = NO;
    // background
    self.mainChartView.drawGridBackgroundEnabled = NO;
    self.mainChartView.drawBarShadowEnabled = NO;
    self.mainChartView.highlightFullBarEnabled = NO;

    // legend && description
    self.mainChartView.legend.enabled = NO;
    self.mainChartView.chartDescription.enabled = NO;
    // provided charts (candle behind line)
    self.mainChartView.drawOrder = @[@(CombinedChartDrawOrderCandle), @(CombinedChartDrawOrderLine)];
    
    // no data text
    self.mainChartView.noDataText = @"";
    
    self.mainChartView.minTopOffset = 15;
    self.mainChartView.minBottomOffset = 0.5;
    
    // gesture properities
    self.mainChartView.dragEnabled = YES;
    [self.mainChartView setScaleXEnabled:YES];
    [self.mainChartView setScaleYEnabled:NO];
    self.mainChartView.autoScaleMinMaxEnabled = YES;
//    [self.mainChartView.viewPortHandler setMinimumScaleX:10];
//    [self.mainChartView.viewPortHandler setMaximumScaleX:50];
//    self.mainChartView.dragDecelerationFrictionCoef = 0.6;
    
    // axis
    self.mainChartView.leftAxis.enabled = NO;
    self.mainChartViewRightAxis = self.mainChartView.rightAxis;
    self.mainChartViewRightAxis.drawGridLinesEnabled = YES;
    self.mainChartViewRightAxis.drawAxisLineEnabled = NO;
    self.mainChartViewRightAxis.enabled = YES;
    self.mainChartViewRightAxis.minWidth = self.mainChartViewRightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    self.mainChartViewRightAxis.labelPosition = YAxisLabelPositionInsideChart;
    self.mainChartViewRightAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    self.mainChartViewRightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    self.mainChartViewRightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    self.mainChartViewRightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    self.mainChartViewRightAxis.axisLineColor = KK_CRYPTO_CHART_AXIS_COLOR;
    [self.mainChartViewRightAxis setLabelCount:4];
    self.mainChartViewRightAxis.forceLabelsEnabled = YES;
    self.mainChartViewRightAxis.drawYaxisLabelLinePostion = YAxisLabelLinePositionLineTop;
    
    ChartXAxis *xAxis = self.mainChartView.xAxis;
    xAxis.drawLabelsEnabled = NO;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
    
    [self initPriceView];
}

- (void)initPriceView {
//    [NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2];
    self.priceLine = [[ChartLimitLine alloc] init];
    self.priceLine.lineWidth = 1.0f;
    self.priceLine.lineDashPhase = 2.0f;
    self.priceLine.lineDashLengths = [NSArray arrayWithObjects:[NSNumber numberWithInt:2], nil];
    [self.mainChartViewRightAxis addLimitLine:self.priceLine];

    self.priceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.priceButton.opaque = 0.8;
    self.priceButton.adjustsImageWhenHighlighted = NO;
    self.priceButton.titleLabel.font = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    [self.priceButton addTarget:self action:@selector(priceButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainChartView addSubview:self.priceButton];
}

- (void)setUpVolumeChartView {
    // delegate
    self.volumeChartView.delegate = self;

    // hightlight
    self.volumeChartView.highlightPerTapEnabled = NO;
    self.volumeChartView.highlightPerDragEnabled = NO;
    
    // background
    self.volumeChartView.drawBarShadowEnabled = NO;
    self.volumeChartView.drawGridBackgroundEnabled = NO;
    self.volumeChartView.highlightFullBarEnabled = NO;

    // legend && description
    self.volumeChartView.legend.enabled = NO;
    self.volumeChartView.chartDescription.enabled = NO;
    
    // provided charts (candle behind line)
    self.volumeChartView.drawOrder = @[@(CombinedChartDrawOrderLine), @(CombinedChartDrawOrderBar)];

    // no data text
    self.volumeChartView.noDataText = @"";
    
    self.volumeChartView.minTopOffset = 0;
    self.volumeChartView.minBottomOffset = 0;

    // gesture properities
    self.volumeChartView.dragEnabled = YES;
    [self.volumeChartView setScaleXEnabled:YES];
    [self.volumeChartView setScaleYEnabled:NO];
    self.volumeChartView.autoScaleMinMaxEnabled = YES;
//    [self.volumeChartView.viewPortHandler setMinimumScaleX:10];
//    [self.volumeChartView.viewPortHandler setMaximumScaleX:50];
    
    // axis
    self.volumeChartView.leftAxis.enabled = NO;
    ChartYAxis *rightAxis = self.volumeChartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.drawAxisLineEnabled = NO;
    [rightAxis setLabelCount:2 force:YES];
    rightAxis.axisMinimum = 0.0;
    rightAxis.minWidth = rightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    rightAxis.labelPosition = YAxisLabelPositionInsideChart;
    rightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    rightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    rightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    rightAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    rightAxis.drawYaxisLabelLinePostion = YAxisLabelLinePositionLineInside;

    ChartXAxis *xAxis = self.volumeChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawLabelsEnabled = YES;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelTextColor = KK_CRYPTO_CHART_XAXIS_TEXT_COLOR;
    xAxis.labelFont = KK_CRYPTO_CHART_XAXIS_TEXT_FONT;
    xAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
}

- (void)setUpIndicatorChartView:(CombinedChartView *)chartView {
    // delegate
    chartView.delegate = self;

    // hightlight
    chartView.highlightPerTapEnabled = NO;
    chartView.highlightPerDragEnabled = NO;
    
    // background
    chartView.drawBarShadowEnabled = NO;
    chartView.drawGridBackgroundEnabled = NO;
    chartView.highlightFullBarEnabled = NO;

    // legend && description
    chartView.legend.enabled = NO;
    chartView.chartDescription.enabled = NO;
    
    // provided charts (bar behind line)
    chartView.drawOrder = @[@(CombinedChartDrawOrderBar), @(CombinedChartDrawOrderLine)];
    
    // no data text
    chartView.noDataText = @"";
    chartView.minBottomOffset = 0;
    chartView.minTopOffset = 0;

    // gesture properities
    chartView.dragEnabled = YES;
    [chartView setScaleXEnabled:YES];
    [chartView setScaleYEnabled:NO];
    chartView.autoScaleMinMaxEnabled = YES;
    
    // axis
    chartView.leftAxis.enabled = NO;
    ChartYAxis *rightAxis = chartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawGridLinesEnabled = YES;
    rightAxis.drawAxisLineEnabled = NO;
    [rightAxis setLabelCount:2 force:YES];
    rightAxis.axisMinimum = 0.0;
    rightAxis.minWidth = rightAxis.maxWidth = KK_CRYPTO_CHART_YAXIS_WIDTH;
    rightAxis.labelPosition = YAxisLabelPositionInsideChart;
    rightAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    rightAxis.labelTextColor = KK_CRYPTO_CHART_YAXIS_TEXT_COLOR;
    rightAxis.labelFont = KK_CRYPTO_CHART_YAXIS_TEXT_FONT;
    rightAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    rightAxis.drawYaxisLabelLinePostion = YAxisLabelLinePositionLineInside;

    ChartXAxis *xAxis = chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    [xAxis setLabelCount: XAXIS_LABEL_COUNT force:YES];
    xAxis.drawLabelsEnabled = NO;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridLineWidth = KK_CRYPTO_CHART_GRID_LINE_WIDTH;
    xAxis.gridColor = KK_CRYPTO_CHART_GRID_LINE_COLOR;
    xAxis.labelTextColor = KK_CRYPTO_CHART_XAXIS_TEXT_COLOR;
    xAxis.labelFont = KK_CRYPTO_CHART_XAXIS_TEXT_FONT;
    xAxis.axisLineColor =KK_CRYPTO_CHART_AXIS_COLOR;
    xAxis.avoidFirstLastClippingEnabled = YES;
    xAxis.drawLimitLinesBehindDataEnabled = YES;
}

- (void)addGesture {
    [self addTouchGesture];
}

- (void)addTouchGesture {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    tapGesture.delegate = self;
    [self addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *mainChartTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(updateInfoLayer:)];
    [self.mainChartView addGestureRecognizer:mainChartTap];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPress:)];
    [self.mainChartView addGestureRecognizer:longPressGesture];
}

#pragma mark - set data

- (void)initConstantData {
    self.dataManager = [KKCryptoChartDataManager shareManager];
    self.moreSelectorTypes = @[@(KKCryptoChartTimeTypeOneMinute), @(KKCryptoChartTimeTypeFiveMinutes), @(KKCryptoChartTimeTypeThirtyMinutes), @(KKCryptoChartTimeTypeOneWeek), @(KKCryptoChartTimeTypeOneMonth), @(KKCryptoChartTimeTypeOneYear)];
    self.showIndicatorChartArray = [[NSMutableArray alloc] initWithObjects:@(KKIndicatorMACD),@(KKIndicatorKDJ),@(KKIndicatorRSI),@(KKIndicatorWR),nil];
    self.currentMainIndType = KKIndicatorNone;
    self.showChartViewArray = [[NSMutableArray alloc] init];
    self.fromSymbol = @"";
}

- (void)initUIStrings {
    self.tabBarTypes = @[@(KKCryptoChartTimeTypeFifteenMinutes), @(KKCryptoChartTimeTypeOneHour), @(KKCryptoChartTimeTypeFourHours), @(KKCryptoChartTimeTypeOneDay), @(KKCryptoChartTabBarMore), @(KKCryptoChartTabBarIndicator)];

    self.tabBarTitles = @[
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_fifteen_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_hour"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_four_hours"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_day"],
        [NSString stringWithFormat:@"%@%@", [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_more"], [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]],
        [NSString stringWithFormat:@"%@%@", [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_indicator"], [self.dataManager getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]]];

    self.moreSelectorTitles = @[
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_minute"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_five_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_thirty_minutes"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_week"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_month"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_time_one_year"]];
}

/**
 call after dataManager set config
 */
- (void)setUpSelectorData {
    [self initUIStrings];
    [self.chartTabBar setupSuffixString:[[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_tab_bar_right_triangle"]];
    [self.chartTabBar setUpTypes:self.tabBarTypes titles:self.tabBarTitles];
    
    //设置more的初始值 title
    if ([[TabbarSelectUtil share]getTabbarSelectBtnTag] == KKCryptoChartTabBarMore){
        NSInteger moreType = [[TabbarSelectUtil share]getTabbarSelectMoreBtnTag];
        if ([self.moreSelectorTypes containsObject:[NSNumber numberWithInteger:moreType]]){
            NSInteger index = [self.moreSelectorTypes indexOfObject:[NSNumber numberWithInteger:moreType]];
            NSString *title = self.moreSelectorTitles[index];
            [self.chartTabBar updateMoreTabItem:KKCryptoChartTabBarMore title:title];
        }
    }
    
    if (self.moreSelector) {
        [self.moreSelector setUpTypes:self.moreSelectorTypes titles:self.moreSelectorTitles];
    }
}

- (void)setConfig:(KKCryptoChartGlobalConfig *)config {
    _config = config;
    
    // 设置chartview的config.timeType 的初始配置
    NSInteger btnType = [[TabbarSelectUtil share]getTabbarSelectBtnTag];
    if (btnType != 0){
        if (btnType == KKCryptoChartTabBarMore){
            btnType = [[TabbarSelectUtil share]getTabbarSelectMoreBtnTag];
        }
        NSString *time = [[KKCryptoChartDataManager shareManager] getTimeString:btnType];
        config.timeType = time;
        _config = config;
    }
    
    //设置 MA默认选中
//    NSInteger status = [[TabbarSelectUtil share] getTabbarIndicatorMainMASelect];
//    if (status == 0) {
//        [[TabbarSelectUtil share] setTabbarIndicatorMainBtnTagWithTag:KKIndicatorMA];
//    }
    
    //设置 IndicatorSelector 的 main和sub 初始值
    NSInteger mainType = [[TabbarSelectUtil share] getTabbarIndicatorMainBtnTag];
    if (mainType != 0){
        [self indicatorSelectorMainChartValueChanged:mainType];
    }
    
    self.showChartViewArray = [[TabbarSelectUtil share] getTabbarIndicatorSubBtnTag].copy;
    
    //RN页面也能切换盘口，刷新当前K线数据。这里做个兼容
    if (self.webSocket) {
        [self updateConfig:config];
    } else {
        __weak typeof(self) weakSelf = self;
        [self fetchCloudData:config completionHandler:^(BOOL loadSuccess) {
            if (loadSuccess) {
                [weakSelf setupWebSocketData:config];
            }
        }];
    }
}

- (void)updateConfig:(KKCryptoChartGlobalConfig *)config {
    [self.mainChartView clear];
    [self.volumeChartView clear];
    
    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        KKIndicatorDescription *descriptionView = [self chartDescriptionFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            descriptionView.hidden = YES;
            [combineView clear];
        }
    }
    
    if (self.webSocket) {
        [self.webSocket close];
        self.webSocket = nil;
    }
    self.mainIndicatorDesc.hidden = YES;
    self.volumeIndicatorDesc.hidden = YES;
    
    __weak typeof(self) weakSelf = self;
    [self fetchCloudData:config completionHandler:^(BOOL loadSuccess) {
        if (loadSuccess) {
            [weakSelf setupWebSocketData:config];
        }
    }];
}

- (void)setUpChartModels:(NSArray<KKCryptoChartModel *> *)models config:(KKCryptoChartGlobalConfig *)config {
    [self generateChartData:models];

    // adjust xAxis min and max
    [self adjustAxis:models];
    
    // description
    [self setupDescription:models.lastObject config:config];

    // visible range (for limit candle width)
    [self.mainChartView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
    [self.mainChartView setVisibleXRangeMaximum:CHART_MAX_VIEW_RANGE];
    [self.volumeChartView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
    [self.volumeChartView setVisibleXRangeMaximum:CHART_MAX_VIEW_RANGE];
    
    // zoom
    [self.mainChartView zoomWithScaleX:[self calMaxScale:models.count] / self.mainChartView.scaleX  scaleY:0 x:0 y:0];
    [self.volumeChartView zoomWithScaleX:[self calMaxScale:models.count] / self.volumeChartView.scaleX scaleY:0 x:0 y:0];

    // move to right

    [self.mainChartView.data notifyDataChanged];
    [self.mainChartView notifyDataSetChanged];

    [self.volumeChartView.data notifyDataChanged];
    [self.volumeChartView notifyDataSetChanged];

    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            [combineView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
            [combineView setVisibleXRangeMaximum:CHART_MAX_VIEW_RANGE];
            [combineView zoomWithScaleX:[self calMaxScale:models.count] / combineView.scaleX scaleY:0 x:0 y:0];
            [combineView.data notifyDataChanged];
            [combineView notifyDataSetChanged];
        }
    }

    [self chartMoveToRight:models isAnimated:NO];
    
    // refresh price line
    [self refreshPriceLine:models.lastObject];
}

- (void)refreshCurrentChartData:(NSArray<KKCryptoChartModel *> *)models
{
    if (models) {
        self.mainData.candleData = [self generateCandleData:models];
        self.mainData.lineData = [self generateLineData:models type:self.currentMainIndType];
        self.mainChartView.data = self.mainData;
        self.mainChartView.xAxis.axisMinimum = self.mainChartView.data.xMin - KKCryptoChartXAxisOffset;
        self.mainChartView.xAxis.axisMaximum = self.mainChartView.data.xMax + KKCryptoChartXAxisRightOffset;
        
        self.volumeData.barData = [self generateBarData:models type:KKIndicatorVolume];
        self.volumeChartView.data = self.volumeData;
        self.volumeChartView.xAxis.axisMinimum = self.volumeChartView.data.xMin - KKCryptoChartXAxisOffset;
        self.volumeChartView.xAxis.axisMaximum = self.volumeChartView.data.xMax + KKCryptoChartXAxisRightOffset;
        
    }
    
    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        CombinedChartData *combineData = [self chartDataFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            combineData.barData = [self generateBarData:models type:currentIndicatorType.integerValue];
            combineData.lineData = [self generateLineData:models type:currentIndicatorType.integerValue];
            [combineView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
            combineView.xAxis.axisMinimum = combineView.data.xMin - KKCryptoChartXAxisOffset;
            combineView.xAxis.axisMaximum = combineView.data.xMax + KKCryptoChartXAxisRightOffset;
            combineView.data = combineData;
            [combineView.data notifyDataChanged];
            [combineView notifyDataSetChanged];
        }
    }
    
    if (self.showChartViewArray.count == 0) {
        [self.volumeChartView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
    }
    
    [self.mainChartView.data notifyDataChanged];
    [self.mainChartView notifyDataSetChanged];

    [self.volumeChartView.data notifyDataChanged];
    [self.volumeChartView notifyDataSetChanged];
    
    // refresh price line
    [self refreshPriceLine:models.lastObject];
    if (self.verticalLineLayer.hidden) {
        [self setupDescription:models.lastObject config:self.config];
    }
}

- (void)setupDescription:(KKCryptoChartModel *)model config:(KKCryptoChartGlobalConfig *)config {
    self.mainIndicatorDesc.hidden = NO;
    self.volumeIndicatorDesc.hidden = NO;
    [self.mainIndicatorDesc setupDescription:model type:self.currentMainIndType config:config];
    [self.volumeIndicatorDesc setupDescription:model type:KKIndicatorVolume config:config];
    
    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        KKIndicatorDescription *descriptionView = [self chartDescriptionFromCurrentTag:currentIndicatorType.integerValue];
        if (descriptionView) {
            descriptionView.hidden = NO;
            [descriptionView setupDescription:model type:currentIndicatorType.integerValue config:config];
        }
    }
}

- (void)chartMoveToRight:(NSArray<KKCryptoChartModel *> *)models isAnimated:(BOOL)isAnimated {
    if (isAnimated) {
        [self.mainChartView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:0.5];
        [self.volumeChartView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:0.5];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView moveViewToAnimatedWithXValue:models.count - 1 yValue:0 axis:AxisDependencyRight duration:0.5];
            }
        }
    } else {
        [self.mainChartView moveViewToX:models.count - 1];
        [self.volumeChartView moveViewToX:models.count - 1];
        
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView moveViewToX:models.count - 1];
            }
        }
    }
}

- (void)refreshPriceLine:(KKCryptoChartModel *)model {
    if (!model) {
        return;
    }

    NSUInteger lowX = round(self.mainChartView.lowestVisibleX);
    NSUInteger highX = ceil(self.mainChartView.highestVisibleX);
    //图表居右时，需要增加对应的偏移量 KKCryptoChartXAxisRightOffset
    if (lowX > highX) {
        return;
    }
    NSInteger location = lowX;
    NSInteger length = highX - lowX;
    self.chartRightEmpty = 0;
    if (self.chartModels.count < highX) {
        //右边有空数据
        NSInteger emptyNum = highX - self.chartModels.count;//空线数量
        self.chartRightEmpty = emptyNum;
        if (emptyNum > 0) {
            lowX = lowX + emptyNum;
            length = highX - lowX;
            location = lowX - emptyNum;
            if (location < 0) {//所有数据小于一屏
                length = self.chartModels.count;
                location = lowX;
                if (location + length > self.chartModels.count) {
                    location = 0;
                }
            }
        }
    }
    
    NSArray<KKCryptoChartModel *> *subArray = [self.chartModels subarrayWithRange:NSMakeRange(location, length)];
    if (subArray.count == 0) {
        return;
    }
    
    //当前屏幕内K线的最大最小值
    CGFloat visibleLowestPrice = subArray.firstObject.low.floatValue, visibleHighestPrice = subArray.firstObject.high.floatValue;
    for (KKCryptoChartModel *model in subArray) {
        if (visibleLowestPrice > model.low.floatValue) {
            visibleLowestPrice = model.low.floatValue;
        }
        if (visibleHighestPrice < model.high.floatValue) {
            visibleHighestPrice = model.high.floatValue;
        }
    }
    if (self.currentMainIndType == KKIndicatorMA || self.currentMainIndType == KKIndicatorEMA || self.currentMainIndType == KKIndicatorBOLL) {
        //当前屏幕内主图数据的最大最小值
        NSArray *mainVisibleArray = [self showScreenMainChartModel:subArray kkIndicatorType:self.currentMainIndType];
        NSString *visibleLowestMain = mainVisibleArray.firstObject;
        if (visibleLowestPrice > visibleLowestMain.floatValue) {
            visibleLowestPrice = visibleLowestMain.floatValue;
        }
        
        NSString *visibleHighestMain = mainVisibleArray.lastObject;
        if (visibleHighestPrice < visibleHighestMain.floatValue) {
            visibleHighestPrice = visibleHighestMain.floatValue;
        }
    }
    
    //Y轴刻度及价格线位置高度(固定最高最低价的在Y轴位置)
    CGFloat space = 15;//上下底部留空像素高度
    CGFloat klineHeight = self.mainChartView.frame.size.height - space * 2;//mainchart高度
    CGFloat visiblePriceSpace = visibleHighestPrice - visibleLowestPrice;
    if (visibleLowestPrice <= 0) {
        return;
    }
    CGFloat pixSpace = klineHeight / visiblePriceSpace;//每一像素占比高度
    CGFloat unitSpace = ((self.mainChartView.frame.size.height / pixSpace) - visiblePriceSpace)/2;//上下底部需要偏移的price
    
    if (unitSpace > 0) {
        visibleHighestPrice = visibleHighestPrice + unitSpace;
        visibleLowestPrice = visibleLowestPrice - unitSpace;
    }
    
    self.visibleLowestPrice = visibleLowestPrice;
    self.visibleHighestPrice = visibleHighestPrice;
    
    self.mainChartViewRightAxis.axisMinimum = visibleLowestPrice;
    self.mainChartViewRightAxis.axisMaximum = visibleHighestPrice;
    if (model.close.floatValue > visibleHighestPrice) {
        self.priceLine.limit = visibleHighestPrice;
    } else if (model.close.floatValue < visibleLowestPrice) {
        self.priceLine.limit = visibleLowestPrice;
    } else {
        self.priceLine.limit = model.close.floatValue;
    }
    
    self.priceLine.lineColor = KK_CRYPTO_CHART_PRICE_LINE_MIDDLE_COLOR;
    // refresh limit label
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:self.config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:self.config.coinPrecision.integerValue];
    NSString *priceString = [formatter stringFromNumber:model.close];
    
    CGFloat labelH = self.priceButton.titleLabel.font.lineHeight;
    CGFloat buttonH = labelH + 4.0f;
    CGRect stringRect = [priceString boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, labelH) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : KK_CRYPTO_CHART_YAXIS_TEXT_FONT} context:nil];
    CGFloat buttonW = stringRect.size.width + 20.0f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *priceLineYpxStr = [NSString stringWithFormat:@"%.4f",self.priceLine.yPx];
        NSString *mainChartHightStr = [NSString stringWithFormat:@"%.4f",CGRectGetHeight(self.mainChartView.frame)];
        //踩坑：ypx是Double类型，CGRectGetHeight是Float类型，不能直接判断是否相等
        if (self.priceLine.yPx > 0 && priceLineYpxStr.floatValue <= mainChartHightStr.floatValue) {
            if (self.chartModels.count >= highX) {
                self.priceButton.frame = CGRectMake(self.mainChartView.viewPortHandler.contentRight - buttonW - 50, self.priceLine.yPx - buttonH / 2, buttonW, buttonH);
                [self.priceButton setTitle:[NSString stringWithFormat:@"%@ ▶︎",priceString] forState:UIControlStateNormal];
                [self.priceButton setTitleColor:KK_CRYPTO_CHART_PRICE_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
                self.priceButton.layer.cornerRadius = buttonH/2;
                self.priceButton.backgroundColor = KK_CRYPTO_CHART_PRICE_LINE_MIDDLE_COLOR;
            } else {
                self.priceButton.hidden = NO;
                self.priceButton.frame = CGRectMake(self.mainChartView.viewPortHandler.contentRight - buttonW + 5, self.priceLine.yPx - buttonH / 2, buttonW, buttonH);
                [self.priceButton setTitle:priceString forState:UIControlStateNormal];
                [self.priceButton setTitleColor:INDICATOR_BUTTON_SELECTED_BACKGROUND_COLOR forState:UIControlStateNormal];
                self.priceButton.backgroundColor = [UIColor clearColor];
            }
        }
    });
}

- (NSArray *)showScreenMainChartModel:(NSArray<KKCryptoChartModel *> *)visibleArray kkIndicatorType:(KKIndicator)type
{
    NSMutableArray *currentArray = [[NSMutableArray alloc] init];//存储主图最大最小值
    switch (type) {
        case KKIndicatorMA:{
            NSArray *firstArray = [[NSArray alloc] initWithObjects:visibleArray.firstObject.MA.MA1,visibleArray.firstObject.MA.MA2,visibleArray.firstObject.MA.MA3, nil];
            CGFloat visibleLowestMA = [[firstArray valueForKeyPath:@"@min.floatValue"] floatValue];
            CGFloat visibleHighestMA = [[firstArray valueForKeyPath:@"@max.floatValue"] floatValue];
            for (KKCryptoChartModel *model in visibleArray) {
                NSArray *allArray = [[NSArray alloc] initWithObjects:model.MA.MA1,model.MA.MA2,model.MA.MA3, nil];
                CGFloat tempLowestMA =[[allArray valueForKeyPath:@"@min.floatValue"] floatValue];
                CGFloat tempHighestMA =[[allArray valueForKeyPath:@"@max.floatValue"] floatValue];
                if (visibleLowestMA > tempLowestMA) {
                    visibleLowestMA = tempLowestMA;
                }
                if (visibleHighestMA < tempHighestMA) {
                    visibleHighestMA = tempHighestMA;
                }
            }
            [currentArray addObject:@(visibleLowestMA)];
            [currentArray addObject:@(visibleHighestMA)];
        }
            break;
        case KKIndicatorEMA:{
            NSArray *firstArray = [[NSArray alloc] initWithObjects:visibleArray.firstObject.EMA.EMA1,visibleArray.firstObject.EMA.EMA2,visibleArray.firstObject.EMA.EMA3, nil];
            CGFloat visibleLowestEMA = [[firstArray valueForKeyPath:@"@min.floatValue"] floatValue];
            CGFloat visibleHighestEMA = [[firstArray valueForKeyPath:@"@max.floatValue"] floatValue];
            for (KKCryptoChartModel *model in visibleArray) {
                NSArray *allArray = [[NSArray alloc] initWithObjects:model.EMA.EMA1,model.EMA.EMA2,model.EMA.EMA3, nil];
                CGFloat tempLowestEMA =[[allArray valueForKeyPath:@"@min.floatValue"] floatValue];
                CGFloat tempHighestEMA =[[allArray valueForKeyPath:@"@max.floatValue"] floatValue];
                if (visibleLowestEMA > tempLowestEMA) {
                    visibleLowestEMA = tempLowestEMA;
                }
                if (visibleHighestEMA < tempHighestEMA) {
                    visibleHighestEMA = tempHighestEMA;
                }
            }
            [currentArray addObject:@(visibleLowestEMA)];
            [currentArray addObject:@(visibleHighestEMA)];
        }
            break;
            
        case KKIndicatorBOLL:{
            NSArray *firstArray = [[NSArray alloc] initWithObjects:visibleArray.firstObject.BOLL.UP,visibleArray.firstObject.BOLL.MID,visibleArray.firstObject.BOLL.LOW, nil];
            CGFloat visibleLowestBOLL = [[firstArray valueForKeyPath:@"@min.floatValue"] floatValue];
            CGFloat visibleHighestBOLL = [[firstArray valueForKeyPath:@"@max.floatValue"] floatValue];
            for (KKCryptoChartModel *model in visibleArray) {
                NSArray *allArray = [[NSArray alloc] initWithObjects:model.BOLL.UP,model.BOLL.MID,model.BOLL.LOW, nil];
                CGFloat tempLowestBOLL =[[allArray valueForKeyPath:@"@min.floatValue"] floatValue];
                CGFloat tempHighestBOLL =[[allArray valueForKeyPath:@"@max.floatValue"] floatValue];
                if (visibleLowestBOLL > tempLowestBOLL) {
                    visibleLowestBOLL = tempLowestBOLL;
                }
                if (visibleHighestBOLL < tempHighestBOLL) {
                    visibleHighestBOLL = tempHighestBOLL;
                }
            }
            [currentArray addObject:@(visibleLowestBOLL)];
            [currentArray addObject:@(visibleHighestBOLL)];
        }
            break;
        default:
            break;
    }
    return currentArray;
}

- (void)generateChartData:(NSArray<KKCryptoChartModel *> *)models {
    // generate main data
    self.mainData = [[CombinedChartData alloc] init];
    self.mainData.candleData = [self generateCandleData:models];
    self.mainData.lineData = [self generateLineData:models type:self.currentMainIndType];
    self.mainChartView.data = self.mainData;
    
    // generate volume data
    self.volumeData = [[CombinedChartData alloc] init];
    self.volumeData.barData = [self generateBarData:models type:KKIndicatorVolume];
    self.volumeChartView.data = self.volumeData;

    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        CombinedChartData *combineData = [self chartDataFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            combineData.barData = [self generateBarData:models type:currentIndicatorType.integerValue];
            combineData.lineData = [self generateLineData:models type:currentIndicatorType.integerValue];
            combineView.data = combineData;
        }
    }
}

// generate line chart data according to indicator model
- (LineChartData *)generateLineData:(NSArray<KKCryptoChartModel *> *)datas type:(KKIndicator)type {
    LineChartData *d = [[LineChartData alloc] init];
    if (type == KKIndicatorNone || type == KKIndicatorTD) {
        return d;
    }
    NSMutableArray *ma1 = [[NSMutableArray alloc] init];
    NSMutableArray *ma2 = [[NSMutableArray alloc] init];
    NSMutableArray *ma3 = [[NSMutableArray alloc] init];
    NSMutableArray *ma4 = [[NSMutableArray alloc] init];

    for (int i = 0; i < datas.count; i++) {
        switch (type) {
            case KKIndicatorMA:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA3 doubleValue]]];
            }
                break;
            case KKIndicatorEMA:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].EMA.EMA3 doubleValue]]];
            }
                break;
            case KKIndicatorBOLL:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.UP doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.MID doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].BOLL.LOW doubleValue]]];
            }
                break;
            case KKIndicatorMACD:{
                NSArray *params = [[KKCryptoChartDataManager shareManager] getIndicatorTimeCycles:KKIndicatorMACD];
                if (i >= [params[1] intValue] - 1) {
                    [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MACD.DIFF doubleValue]]];
                }
                
                if (i >= [params[1] intValue] + [params[2] intValue] - 1) {
                    [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MACD.DEA doubleValue]]];
                }
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:([datas[i].MACD.DIFF doubleValue] + [datas[i].MACD.DEA doubleValue])/2]];
            }
                break;
            case KKIndicatorKDJ:{
                NSArray *params = [[KKCryptoChartDataManager shareManager] getIndicatorTimeCycles:KKIndicatorKDJ];
                if (i >= [params[0] intValue] - 1) {
                    [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.K doubleValue]]];
                    [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.D doubleValue]]];
                    [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].KDJ.J doubleValue]]];
                }
                //占位
                [ma4 addObject:[[ChartDataEntry alloc] initWithX:i y:([datas[i].KDJ.K doubleValue] + [datas[i].KDJ.D doubleValue] + [datas[i].KDJ.J doubleValue])/3]];
            }
                break;
            case KKIndicatorRSI:{
                NSArray *params = [[KKCryptoChartDataManager shareManager] getIndicatorTimeCycles:KKIndicatorRSI];
                if (i >= [params[0] intValue] - 1) {
                    [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI1 doubleValue]]];
                }
                if (i >= [params[1] intValue] - 1) {
                    [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI2 doubleValue]]];
                }
    
                if (i >= [params[2] intValue] - 1) {
                    [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].RSI.RSI3 doubleValue]]];
                }
                //占位
                [ma4 addObject:[[ChartDataEntry alloc] initWithX:i y:([datas[i].RSI.RSI1 doubleValue] + [datas[i].RSI.RSI2 doubleValue] + [datas[i].RSI.RSI3 doubleValue])/3]];

            }
                break;
            case KKIndicatorWR:{
                NSArray *params = [[KKCryptoChartDataManager shareManager] getIndicatorTimeCycles:KKIndicatorWR];
                if (i >= [params[0] intValue] - 1) {
                    [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].WR.WR1 doubleValue]]];
                }
                if (i >= [params[1] intValue] - 1) {
                    [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].WR.WR2 doubleValue]]];
                }
                //占位
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:([datas[i].WR.WR1 doubleValue] + [datas[i].WR.WR2 doubleValue])/2]];
            }
                break;
            default:{
                [ma1 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA1 doubleValue]]];
                [ma2 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA2 doubleValue]]];
                [ma3 addObject:[[ChartDataEntry alloc] initWithX:i y:[datas[i].MA.MA3 doubleValue]]];
            }
                break;
        }
        
    }
    
    switch (type) {
        case KKIndicatorMA:
        case KKIndicatorEMA:
        case KKIndicatorBOLL:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:YES];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:YES];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma3 isMainChartLine:YES];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
        }
            break;
        case KKIndicatorMACD:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:[UIColor clearColor] entries:ma3 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];//占位
        }
            break;
        case KKIndicatorKDJ:
        case KKIndicatorRSI:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma3 isMainChartLine:NO];
            LineChartDataSet *set4 = [self setupLineChartDataSet:[UIColor clearColor] entries:ma4 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
            [d addDataSet:set4];//占位
        }
            break;
        case KKIndicatorWR:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma2 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:[UIColor clearColor] entries:ma3 isMainChartLine:NO];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];//占位
        }
            break;
        default:{
            LineChartDataSet *set1 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_1_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set2 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_2_COLOR entries:ma1 isMainChartLine:NO];
            LineChartDataSet *set3 = [self setupLineChartDataSet:KK_CRYPTO_CHART_LINE_3_COLOR entries:ma1 isMainChartLine:YES];
            [d addDataSet:set1];
            [d addDataSet:set2];
            [d addDataSet:set3];
        }
            break;
    }
    
    return d;
}

// common method: setup line chart style
- (LineChartDataSet *)setupLineChartDataSet:(UIColor *)color entries:(NSArray<ChartDataEntry *> *)entries isMainChartLine:(BOOL)isMainChartLine {
    LineChartDataSet *set = [[LineChartDataSet alloc] initWithEntries:entries label:nil];
    [set setColor:color];
    set.lineWidth = 1.0;
    if (isMainChartLine) {
        set.mode = LineChartModeCubicBezier;
    }
    set.drawValuesEnabled = NO;
    set.axisDependency = AxisDependencyRight;
    set.drawCirclesEnabled = NO;
    set.highlightEnabled = NO;
    return set;
}

- (CandleChartData *)generateCandleData:(NSArray<KKCryptoChartModel *> *)datas {
    NSMutableArray *entriesBuy = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSell = [[NSMutableArray alloc] init];
    NSMutableArray *entriesBuyNinth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSellNinth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesBuyThirteenth = [[NSMutableArray alloc] init];
    NSMutableArray *entriesSellThirteenth = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < datas.count; i++) {
        KKCryptoChartModel *data = datas[i];
        double high = [data.high doubleValue];
        double low = [data.low doubleValue];
        double open = [data.open doubleValue];
        double close = [data.close doubleValue];
        if (data.TD && data.TD.buySetupIndex > 0) {
            if (data.TD.buySetupIndex.integerValue == 9) {
                UIImage *image = [UIImage imageNamed:@"td_buy_ninth" inBundle:[BundleUtil getBundle] compatibleWithTraitCollection:nil];
                [entriesBuyNinth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:image data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else if (data.TD.buySetupIndex.integerValue == 13) {
                [entriesBuyThirteenth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else {
                [entriesBuy addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            }
            
        } else {
            if (data.TD.sellSetupIndex.integerValue == 9) {
                UIImage *image = [UIImage imageNamed:@"td_sell_ninth" inBundle:[BundleUtil getBundle] compatibleWithTraitCollection:nil];
                [entriesSellNinth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:image data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else if (data.TD.sellSetupIndex.integerValue == 13) {
                [entriesSellThirteenth addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            } else {
                [entriesSell addObject:[[CandleChartDataEntry alloc] initWithX:i shadowH:high shadowL:low open:open close:close icon:nil data:data.TD precision:self.config.coinPrecision.integerValue]];
            }
            
        }
        
    }
    CandleChartData *d = [[CandleChartData alloc] initWithDataSets:@[[self setUpCandleDataSet:entriesBuy tdIndicatorType:KKTDIndicatorTypeBuy], [self setUpCandleDataSet:entriesBuyNinth tdIndicatorType:KKTDIndicatorTypeBuyNinth], [self setUpCandleDataSet:entriesBuyThirteenth tdIndicatorType:KKTDIndicatorTypeBuyThirteenth], [self setUpCandleDataSet:entriesSell tdIndicatorType:KKTDIndicatorTypeSell], [self setUpCandleDataSet:entriesSellNinth tdIndicatorType:KKTDIndicatorTypeSellNinth], [self setUpCandleDataSet:entriesSellThirteenth tdIndicatorType:KKTDIndicatorTypeSellThirteenth]]];
    return d;
}

- (CandleChartDataSet *)setUpCandleDataSet:(NSArray<CandleChartDataEntry *> *)entries tdIndicatorType:(KKTDIndicatorType)type {
    CandleChartDataSet *candleSet = [[CandleChartDataSet alloc] initWithEntries:entries label:nil];
    candleSet.drawVerticalHighlightIndicatorEnabled = NO;
    candleSet.drawHorizontalHighlightIndicatorEnabled = NO;
    candleSet.valueFormatter = [[TDIndicatorValueFormatter alloc] init];
    candleSet.valueFont = [UIFont systemFontOfSize:9.f];
    switch (type) {
        case KKTDIndicatorTypeBuy:
            candleSet.valueTextColor = KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR;
            break;
        case KKTDIndicatorTypeSell:
            candleSet.valueTextColor = KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;
            break;
        default:
            break;
    }
    
    if (self.currentMainIndType == KKIndicatorTD) {
        if (type == KKTDIndicatorTypeBuy || type == KKTDIndicatorTypeSell) {
            candleSet.drawIconsEnabled = NO;
            candleSet.drawValuesEnabled = YES;
        } else {
            candleSet.drawIconsEnabled = YES;
            candleSet.drawValuesEnabled = NO;
            candleSet.iconsOffset = CGPointMake(0, -15);
        }
    } else {
        candleSet.drawIconsEnabled = NO;
        candleSet.drawValuesEnabled = NO;
        candleSet.iconsOffset = CGPointMake(0, -15);
    }
    
//    candleSet.valueTextColor = isBuySetup ? KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR : KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;

    // color
    candleSet.shadowColorSameAsCandle = YES;
    candleSet.shadowWidth = KK_CRYPTO_CHART_CANDLE_SHADOW_LINE_WIDTH;

    // fix charts framework color error
    candleSet.decreasingColor = KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR;
    candleSet.decreasingFilled = YES;
    candleSet.increasingColor = KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;
    candleSet.increasingFilled = YES;
    candleSet.neutralColor = KK_CRYPTO_CHART_CANDLE_NEUTRAL_COLOR;
    
    // axis
    candleSet.axisDependency = AxisDependencyRight;
    
    // highlight
    candleSet.highlightEnabled = YES;
    
    return candleSet;
}

// KKIndicatorNone:Volume data; KKIndicatorMACD: MACD bar data
- (BarChartData *)generateBarData:(NSArray<KKCryptoChartModel *> *)datas type:(KKIndicator)type {
    BarChartData *d = [[BarChartData alloc] init];
    if ((type != KKIndicatorVolume) && (type != KKIndicatorMACD)) {
        return d;
    }
    NSMutableArray<BarChartDataEntry *> *entries1 = [[NSMutableArray alloc] init];
    NSMutableArray<BarChartDataEntry *> *entries2 = [[NSMutableArray alloc] init];
    for (int index = 0; index < datas.count; index++) {
        switch (type) {
            case KKIndicatorVolume:{
                if ([datas[index].open doubleValue] <= [datas[index].close doubleValue]) {
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                } else {
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
            }
                break;
            case KKIndicatorMACD:{
                if ([datas[index].MACD.MACD doubleValue] > 0) {
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].MACD.MACD doubleValue]]];
                } else {
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].MACD.MACD doubleValue]]];
                }
            }
                break;
            default:{
                if ([datas[index].open doubleValue] < [datas[index].close doubleValue]) {
                    [entries1 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                } else {
                    [entries2 addObject:[[BarChartDataEntry alloc] initWithX:index y:[datas[index].volume doubleValue]]];
                }
                break;
            }
        }
    }

    // setup increase style, fix charts framework color error

    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithEntries:entries1 label:nil];
    [set1 setColor:KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR];
    set1.drawValuesEnabled = NO;
    set1.axisDependency = AxisDependencyRight;
    
    // setup decrese style
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithEntries:entries2 label:nil];
    [set2 setColor:KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR];
    set2.drawValuesEnabled = NO;
    set2.axisDependency = AxisDependencyRight;

    d = [[BarChartData alloc] initWithDataSets:@[set1, set2]];
    
    return d;
}

- (void)adjustAxis:(NSArray<KKCryptoChartModel *> *)models {
    self.mainChartView.xAxis.axisMinimum = self.mainChartView.data.xMin - KKCryptoChartXAxisOffset;
    self.mainChartView.xAxis.axisMaximum = self.mainChartView.data.xMax + KKCryptoChartXAxisRightOffset;
    self.volumeChartView.xAxis.axisMinimum = self.volumeChartView.data.xMin - KKCryptoChartXAxisOffset;
    self.volumeChartView.xAxis.axisMaximum = self.volumeChartView.data.xMax + KKCryptoChartXAxisRightOffset;
    NSInteger b = [self.config.coinPrecision integerValue];
    self.mainChartViewRightAxis.valueFormatter = [[PriceValueFormatter alloc] initWithPrecision:b];
    self.volumeChartView.rightAxis.valueFormatter = [[PriceValueFormatter alloc] initWithPrecision:2];
    if (self.showChartViewArray.count == 0) {
        self.volumeChartView.xAxis.drawLabelsEnabled = YES;
        self.volumeChartView.xAxis.drawAxisLineEnabled = YES;
        [self.volumeChartView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
    } else {
        self.volumeChartView.xAxis.drawLabelsEnabled = NO;
        self.volumeChartView.xAxis.drawAxisLineEnabled = NO;
    }
    
    for (int i = 0; i < self.showChartViewArray.count; i++) {
        NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
        CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
        if (combineView) {
            combineView.xAxis.axisMinimum = combineView.data.xMin - KKCryptoChartXAxisOffset;
            combineView.xAxis.axisMaximum = combineView.data.xMax + KKCryptoChartXAxisRightOffset;
            if (currentIndicatorType.integerValue == KKIndicatorMACD) {
                combineView.rightAxis.valueFormatter = [[PriceValueFormatter alloc] initWithPrecision:b];
            } else {
                combineView.rightAxis.valueFormatter = [[PriceValueFormatter alloc] initWithPrecision:2];
            }

            if (i == 0) {
                combineView.minTopOffset = 0.5;
            }
            //坐标轴只绘制最后一组
            if (i == self.showChartViewArray.count - 1) {
                combineView.xAxis.drawLabelsEnabled = YES;
                combineView.xAxis.drawAxisLineEnabled = YES;
                [combineView.xAxis setValueFormatter:[self setUpXAxisValueFormatter:models]];
            } else {
                combineView.xAxis.drawLabelsEnabled = NO;
                combineView.xAxis.drawAxisLineEnabled = NO;
            }
            [combineView.rightAxis resetCustomAxisMin];
        }
    }
}

- (ChartDefaultAxisValueFormatter *)setUpXAxisValueFormatter:(NSArray<KKCryptoChartModel *> *)models {
    __weak typeof(self) weakSelf = self;
    ChartDefaultAxisValueFormatter *formatter = [[ChartDefaultAxisValueFormatter alloc] initWithBlock:^NSString * _Nonnull(double value, ChartAxisBase * _Nullable axis) {
        // fix x axis overlap
        if (models.count <= 2) {
            value = value + KKCryptoChartXAxisOffset;
        }
        if ((NSInteger)value >= models.count) {
            return @"";
        }
        NSNumber *timestamp = models[(NSInteger)value].timestamp;
        return [weakSelf getFormatterStringFromTimeType:models[(NSInteger)value].timeType timestamp:timestamp];
    }];
    return formatter;
}

- (NSString *)getFormatterStringFromTimeType:(NSString *)timeType timestamp:(NSNumber *)timestamp {
    KKCryptoChartTimeType type = [self.dataManager getTimeTypeEnum:timeType];
    NSString *formatteredValue = nil;
    switch (type) {
        case KKCryptoChartTimeTypeOneMinute:
            formatteredValue = [DateUtil getHourTimeFromTimestamp:timestamp.stringValue];
            break;
        case KKCryptoChartTimeTypeFiveMinutes:
        case KKCryptoChartTimeTypeFifteenMinutes:
        case KKCryptoChartTimeTypeThirtyMinutes:
        case KKCryptoChartTimeTypeOneHour:
        case KKCryptoChartTimeTypeFourHours:
            formatteredValue = [DateUtil getHourMinuteFromTimestamp:timestamp.stringValue];
            break;
        case KKCryptoChartTimeTypeOneDay:
        case KKCryptoChartTimeTypeOneWeek:
        case KKCryptoChartTimeTypeOneMonth:
            formatteredValue = [DateUtil getMonthDayFromTimestamp:timestamp.stringValue];
            break;
        case KKCryptoChartTimeTypeOneYear:
            formatteredValue = [DateUtil getYearFromTimestamp:timestamp.stringValue];
            break;
        default:
            formatteredValue = [DateUtil getHourMinuteFromTimestamp:timestamp.stringValue];
            break;
    }
    return formatteredValue;
}

- (CGFloat)calMaxScale:(CGFloat)count {
    return count / CANDLE_CHART_CANDLE_COUNT;
}

#pragma mark - network
- (void)fetchCloudData:(KKCryptoChartGlobalConfig*)config completionHandler:(void (^)(BOOL loadSuccess))completionHandler{
    self.dataManager.config = config;
    [self setUpSelectorData];
    __strong typeof(self) weakSelf = self;
    [self.loadingView startAnimating];
    [self.dataManager fetchCloudDataWithCoinCode:config.coinCode timeType:config.timeType environment:config.environment completionHandler:^(NSArray<KKCryptoChartModel *> * _Nonnull chartModels, NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.loadingView stopAnimating];
            if (chartModels) {
                weakSelf.chartModels = chartModels;
                [weakSelf setUpChartModels:chartModels config:config];
                completionHandler(YES);
            }
            if (error) {
                if (weakSelf.bugReportDelegate && [weakSelf.bugReportDelegate respondsToSelector:@selector(didErrorOccurred:error:)]) {
                    [weakSelf.bugReportDelegate didErrorOccurred:weakSelf error:error];
                }
            }
        });
    }];
}

- (void)setupWebSocketData:(KKCryptoChartGlobalConfig *)config {
    NSString *url;
    if (config.environment == KKCryptoChartEnvironmentTypeDev) {
        url = KK_WEB_SOCKET_DEV_URL;
    } else if (config.environment == KKCryptoChartEnvironmentTypeBeta) {
        url = KK_WEB_SOCKET_BETA_URL;
    } else {
        url = KK_WEB_SOCKET_PROD_URL;
    }
    if (!self.webSocket) {
        KKCryptoChartDataManager *manager = [KKCryptoChartDataManager shareManager];
        NSString *resolution = [manager getSocketResolution:[manager getTimeTypeEnum:config.timeType]];
        self.webSocket = [[KKWebSocket alloc] initWithURLString:url symbol:config.coinCode fromSymbol:self.fromSymbol resolution:resolution];
        [self.webSocket open];
        self.webSocket.delegate = self;
        self.fromSymbol = config.coinCode;
    }
}

#pragma mark - ChartViewDelegate

- (void)chartScaled:(ChartViewBase *)chartView scaleX:(CGFloat)scaleX scaleY:(CGFloat)scaleY {
    [self showCrossLine:NO];
    CGAffineTransform currentMatrix = chartView.viewPortHandler.touchMatrix;
    if (chartView == self.mainChartView) {
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    } else if (chartView == self.volumeChartView) {
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    } else {
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView && chartView != combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    }
    
    // refresh price line
    [self refreshPriceLine:self.chartModels.lastObject];
}

- (void)chartTranslated:(ChartViewBase *)chartView dX:(CGFloat)dX dY:(CGFloat)dY {
//    KKLog(@"chartTranslated triggered, chartView:%@", chartView.description);
    [self showCrossLine:NO];
    
    // sync multi-chart
    CGAffineTransform currentMatrix = chartView.viewPortHandler.touchMatrix;
    if (chartView == self.mainChartView) {
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    } else if (chartView == self.volumeChartView) {
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    } else {
        [self.mainChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.mainChartView invalidate:YES];
        [self.volumeChartView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:self.volumeChartView invalidate:YES];
        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView && chartView != combineView) {
                [combineView.viewPortHandler refreshWithNewMatrix:currentMatrix chart:combineView invalidate:YES];
            }
        }
    }
    
    // refresh price line
    [self refreshPriceLine:self.chartModels.lastObject];
}

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight {
    KKLog(@"chartValueSelected, entry:%@,%@", entry.description,highlight);
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView {
    KKLog(@"chartValueNothingSelected");
}
//停止移动时调用
- (void)chartViewDidEndPanning:(ChartViewBase *)chartView{
    KKLog(@"chartViewDidEndPanning");
//    [self showCrossLine:NO];
}

#pragma mark - KKCryptoChartTabBarDelegate

- (void)didTabItemSelected:(NSInteger)index title:(NSString *)title {
    KKLog(@"didTabItemSelected, index=%ld, title=%@", (long)index, title);
    if (index == KKCryptoChartTabBarMore) {
        self.indicatorSelector.hidden = YES;
        if (!self.moreSelector) {
            self.moreSelector = [[KKCryptoMoreSelector alloc] init];
            [self.moreSelector setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame) + 12, self.frame.size.width, 30)];
            [self.moreSelector setUpTypes:self.moreSelectorTypes titles:self.moreSelectorTitles];
            self.moreSelector.delegate = self;
            [self addSubview:self.moreSelector];
            self.moreSelector.hidden = YES;
        }
        self.moreSelector.hidden = !self.moreSelector.hidden;
    } else if (index == KKCryptoChartTabBarIndicator) {
        self.moreSelector.hidden = YES;
        if (!self.indicatorSelector) {
            self.indicatorSelector = [[KKIndicatorSelector alloc] init];
            [self.indicatorSelector setFrame:CGRectMake(0, CGRectGetMaxY(self.chartTabBar.frame) + 12, self.frame.size.width, 60)];
            self.indicatorSelector.delegate = self;
            [self addSubview:self.indicatorSelector];
            [self.indicatorSelector setUpData];
            self.indicatorSelector.hidden = YES;
        }
        self.indicatorSelector.hidden = !self.indicatorSelector.hidden;
    } else {
        self.priceButton.hidden = YES;
        self.moreSelector.hidden = YES;
        [self.moreSelector setAllBtnUnselected];
        NSString *timeType = [self.dataManager getTimeString:index];
        if (timeType) {
            self.config.timeType = timeType;
            [self updateConfig:self.config];
        }
    }
    
    if (self.infoLayer.hidden == NO) {
        [self showCrossLine:NO];
    }
}

#pragma mark - KKIndicatorSelectorDelegate

- (void)indicatorSelectorMainChartValueChanged:(KKIndicator)currentIndicator {
    self.currentMainIndType = currentIndicator;
    if (!self.chartModels) {
        return;
    }
    [[TabbarSelectUtil share] setTabbarIndicatorMainMASelectWithStatus:1];
    switch (currentIndicator) {
        case KKIndicatorTD: {
            self.mainData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            [self.mainData.candleData.dataSets enumerateObjectsUsingBlock:^(id<IChartDataSet>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (CGColorEqualToColor(obj.valueTextColor.CGColor, KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR.CGColor)) {
                    obj.drawValuesEnabled = YES;
                    obj.drawIconsEnabled = NO;
                } else if (CGColorEqualToColor(obj.valueTextColor.CGColor, KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR.CGColor)) {
                    obj.drawValuesEnabled = YES;
                    obj.drawIconsEnabled = NO;
                } else {
                    obj.drawValuesEnabled = NO;
                    obj.drawIconsEnabled = YES;
                }
                
            }];
            self.mainChartView.data = self.mainData;
            [self.mainChartView.data notifyDataChanged];
            [self.mainChartView notifyDataSetChanged];
        }
            break;
        case KKIndicatorMA:
        case KKIndicatorEMA:
        case KKIndicatorBOLL:
        case KKIndicatorNone:
        default:{
            self.mainData.lineData = [self generateLineData:self.chartModels type:currentIndicator];
            [self.mainData.candleData.dataSets enumerateObjectsUsingBlock:^(id<IChartDataSet>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.drawValuesEnabled = NO;
            }];
            self.mainChartView.data = self.mainData;
            [self.mainChartView.data notifyDataChanged];
            [self.mainChartView notifyDataSetChanged];
        }
            break;
    }
    [self.mainIndicatorDesc setupDescription:self.chartModels.lastObject type:currentIndicator config:self.config];
    [self refreshPriceLine:self.chartModels.lastObject];
}

- (void)indicatorSelectorSideChartValueChanged:(NSArray *)indicatorArray {
    self.showChartViewArray = indicatorArray.mutableCopy;
    //不在数组内的副图需要移除销毁
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",indicatorArray];
    NSArray *filterArray = [self.showIndicatorChartArray filteredArrayUsingPredicate:filterPredicate];//当前未选中的副图
    for (int i = 0; i < filterArray.count; i++) {
        NSString *indicatorType = [filterArray objectAtIndex:i];
        [self removeIndicatorChartView:indicatorType.integerValue];
    }
    
    for (int i = 0; i < indicatorArray.count; i ++) {
        NSString *currentIndicator = [indicatorArray objectAtIndex:i];
        [self addIndicatorChartView:currentIndicator.integerValue];
    }
    
    if (self.showChartViewArray.count == 0) {
        [self addIndicatorChartView:KKIndicatorNone];
    }
}
    
- (void)addIndicatorChartView:(KKIndicator)currentIndicator {
    [self setNeedsLayout];
    switch (currentIndicator) {
        case KKIndicatorKDJ:
            self.kdjIndicatorData.lineData = [self generateLineData:self.chartModels type:KKIndicatorKDJ];
            self.kdjIndicatorData.barData = [[BarChartData alloc] init];
            self.kdjChartView.data = self.kdjIndicatorData;
            self.kdjIndicatorDesc.hidden = NO;
            [self.kdjIndicatorDesc setupDescription:self.chartModels.lastObject type:KKIndicatorKDJ config:self.config];
            break;
        case KKIndicatorRSI:
            self.rsiIndicatorData.lineData = [self generateLineData:self.chartModels type:KKIndicatorRSI];
            self.rsiIndicatorData.barData = [[BarChartData alloc] init];
            self.rsiChartView.data = self.rsiIndicatorData;
            self.rsiIndicatorDesc.hidden = NO;
            [self.rsiIndicatorDesc setupDescription:self.chartModels.lastObject type:KKIndicatorRSI config:self.config];
            break;
        case KKIndicatorWR:{
            self.wrIndicatorData.lineData = [self generateLineData:self.chartModels type:KKIndicatorWR];
            self.wrIndicatorData.barData = [[BarChartData alloc] init];
            self.wrChartView.data = self.wrIndicatorData;
            self.wrIndicatorDesc.hidden = NO;
            [self.wrIndicatorDesc setupDescription:self.chartModels.lastObject type:KKIndicatorWR config:self.config];
        }
            break;
        case KKIndicatorMACD:{
            self.indicatorData.lineData = [self generateLineData:self.chartModels type:KKIndicatorMACD];
            self.indicatorData.barData = [self generateBarData:self.chartModels type:KKIndicatorMACD];
            self.indicatorChartView.data = self.indicatorData;
            self.sideIndicatorDesc.hidden = NO;
            [self.sideIndicatorDesc setupDescription:self.chartModels.lastObject type:KKIndicatorMACD config:self.config];
        }
            break;
        case KKIndicatorNone:
        default:
            break;
    }
    [self adjustAxis:self.chartModels];
    CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicator];
    if (combineView) {
        [combineView setVisibleXRangeMinimum:CHART_MIN_VIEW_RANGE];
        [combineView setVisibleXRangeMaximum:CHART_MAX_VIEW_RANGE];
        [combineView zoomWithScaleX:[self calMaxScale:self.chartModels.count] / combineView.scaleX scaleY:0 x:0 y:0];
        [combineView.data notifyDataChanged];
        [combineView notifyDataSetChanged];
        if (self.chartRightEmpty > 0) {
            [self chartMoveToRight:self.chartModels isAnimated:NO];
            [combineView.viewPortHandler refreshWithNewMatrix:self.mainChartView.viewPortHandler.touchMatrix chart:combineView invalidate:YES];
        } else {
            [combineView.viewPortHandler refreshWithNewMatrix:self.mainChartView.viewPortHandler.touchMatrix chart:combineView invalidate:YES];
        }
    }
}

- (void)removeIndicatorChartView:(KKIndicator)currentIndicator
{
    switch (currentIndicator) {
        case KKIndicatorMACD:{
            if (_indicatorChartView) {
                [_indicatorChartView clear];
                [_indicatorChartView removeFromSuperview];
                _indicatorChartView = nil;
                _sideIndicatorDesc = nil;
            }
        }
            break;
        case KKIndicatorKDJ:{
            if (_kdjChartView) {
                [_kdjChartView clear];
                [_kdjChartView removeFromSuperview];
                _kdjChartView = nil;
                _kdjIndicatorDesc = nil;
            }
        }
            break;
        case KKIndicatorRSI: {
            if (_rsiChartView) {
                [_rsiChartView clear];
                [_rsiChartView removeFromSuperview];
                _rsiChartView = nil;
                _rsiIndicatorDesc = nil;
            }
        }
            break;
        case KKIndicatorWR:{
            if (_wrChartView) {
                [_wrChartView clear];
                [_wrChartView removeFromSuperview];
                _wrChartView = nil;
                _wrIndicatorDesc = nil;
            }
        }
            break;
        default:
            break;
    }
}

- (CombinedChartView *)chartViewFromCurrentTag:(KKIndicator)currentIndicator {
    switch (currentIndicator) {
        case KKIndicatorMACD:
            return self.indicatorChartView;
            break;
        case KKIndicatorKDJ:
            return self.kdjChartView;
            break;
        case KKIndicatorRSI:
            return self.rsiChartView;
            break;
        case KKIndicatorWR:
            return self.wrChartView;
            break;
        default:
            return nil;
            break;
    }
    return nil;
}

- (CombinedChartData *)chartDataFromCurrentTag:(KKIndicator)currentIndicator {
    switch (currentIndicator) {
        case KKIndicatorMACD:
            return self.indicatorData;
            break;
        case KKIndicatorKDJ:
            return self.kdjIndicatorData;
            break;
        case KKIndicatorRSI:
            return self.rsiIndicatorData;
            break;
        case KKIndicatorWR:
            return self.wrIndicatorData;
            break;
        default:
            return nil;
            break;
    }
    return nil;
}

- (KKIndicatorDescription *)chartDescriptionFromCurrentTag:(KKIndicator)currentIndicator {
    switch (currentIndicator) {
        case KKIndicatorMACD:
            return self.sideIndicatorDesc;
            break;
        case KKIndicatorKDJ:
            return self.kdjIndicatorDesc;
            break;
        case KKIndicatorRSI:
            return self.rsiIndicatorDesc;
            break;
        case KKIndicatorWR:
            return self.wrIndicatorDesc;
            break;
        default:
            return nil;
            break;
    }
    return nil;
}

#pragma mark - KKMoreSelectorDelegate

- (void)didMoreSelectorSelected:(NSInteger)type title:(NSString *)title {
    KKLog(@"didMoreSelectorSelected type=%ld, title=%@", (long)type, title);
    self.moreSelector.hidden = YES;
    self.priceButton.hidden = YES;
    [self.chartTabBar updateMoreTabItem:KKCryptoChartTabBarMore title:title];
    self.config.timeType = [self.dataManager getTimeString:type];
    [self updateConfig:self.config];
    //如果moreview点击了,把tabbar的more按钮设置为选中
    [[TabbarSelectUtil share] setTabbarSelectBtnTagWithTag:KKCryptoChartTabBarMore];
    
}

#pragma mark - KKWebSocketDelegate

- (void)didReceiveKlineData:(NSDictionary *)klineData {
    if (!klineData) {
        return;
    }
    if (!self.chartModels || self.chartModels.count == 0) {
        return;
    }
    KKCryptoChartModel *model = [self setupWebSocketModel:klineData];
    if (!model.timestamp) {
        return;
    }
    NSMutableArray *mutArray = [[NSMutableArray alloc] initWithArray:self.chartModels];
    KKCryptoChartTimeType socketType = [self.dataManager getTimeTypeEnum:model.timeType];
    KKCryptoChartTimeType selectType = [self.dataManager getTimeTypeEnum:self.config.timeType];
    if (model.timestamp.integerValue >= self.chartModels.lastObject.timestamp.integerValue && socketType == selectType) {
        if ([self checkSameTime:model.timestamp.stringValue toTimestamp:self.chartModels.lastObject.timestamp.stringValue]) {
            //需要根据维度更新显示的时间戳
            model.timestamp = self.chartModels.lastObject.timestamp;
            [mutArray replaceObjectAtIndex:mutArray.count - 1 withObject:model];
            self.chartModels = [NSArray arrayWithArray:mutArray];
            if (!self.verticalLineLayer.hidden && [self checkSameTime:model.timestamp.stringValue toTimestamp:self.currentChartModel.timestamp.stringValue]) {
                [self.infoLayer updateAlertValueWithModel:model config:self.config];
            }
        } else {
            BOOL isAdd = [self checkAddTime:model.timestamp.stringValue lastTimeStamp:self.chartModels.lastObject.timestamp.stringValue];
            if (isAdd) {
                [mutArray addObject:model];
                self.chartModels = [NSArray arrayWithArray:mutArray];
                if (self.highlight && !self.verticalLineLayer.hidden && !self.isLongPress) {
                    //十字线显示状态下，需要跟随柱子移动
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        CGPoint currenthigtlightPoint = [self.mainChartView getMarkerPositionWithHighlight:self.highlight];
                        //十字线超过左边界的话直接隐藏
                        if (currenthigtlightPoint.x < 10) {
                            [self showCrossLine:NO];
                        } else {
                            [self drawCrossLine:CGPointMake(currenthigtlightPoint.x, self.highLightY)];
                            [self drawVolumeText:CGPointMake(currenthigtlightPoint.x, 0) model:self.currentChartModel];
                        }
                    });
                }
            }
        }

        [[KKCryptoChartDataManager shareManager] calculateIndicatorData:self.chartModels];
        [self refreshCurrentChartData:self.chartModels];
        
    }
    
}

- (BOOL)checkSameTime:(NSString *)fromTimestamp toTimestamp:(NSString *)toTimestamp {
    BOOL isSameTime = NO;
    KKCryptoChartDataManager *manager = [KKCryptoChartDataManager shareManager];
    KKCryptoChartTimeType timeType = [manager getTimeTypeEnum:self.config.timeType];
    if (timeType == KKCryptoChartTimeTypeOneMinute) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:1];
    } else if (timeType == KKCryptoChartTimeTypeFiveMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:5];
    } else if (timeType == KKCryptoChartTimeTypeFifteenMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:15];
    } else if (timeType == KKCryptoChartTimeTypeThirtyMinutes) {
        isSameTime = [DateUtil isSameMinuteInterval:fromTimestamp toTimestamp:toTimestamp interval:30];
    } else if (timeType == KKCryptoChartTimeTypeOneHour) {
        isSameTime = [DateUtil isSameHourInterval:fromTimestamp toTimestamp:toTimestamp interval:1];
    } else if (timeType == KKCryptoChartTimeTypeFourHours) {
        isSameTime = [DateUtil isSameHourInterval:fromTimestamp toTimestamp:toTimestamp interval:4];
    } else if (timeType == KKCryptoChartTimeTypeOneDay) {
        isSameTime = [DateUtil isSameDay:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneWeek) {
        isSameTime = [DateUtil isSameWeek:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneMonth) {
        isSameTime = [DateUtil isSameMonth:fromTimestamp toTimestamp:toTimestamp];
    } else if (timeType == KKCryptoChartTimeTypeOneYear) {
        isSameTime = [DateUtil isSameYear:fromTimestamp toTimestamp:toTimestamp];
    }
    return isSameTime;
}
//检查是否需要添加时间线
- (BOOL)checkAddTime:(NSString *)socketTimeStamp lastTimeStamp:(NSString *)lastTimeStamp
{
    BOOL isAdd = NO;
    KKCryptoChartDataManager *manager = [KKCryptoChartDataManager shareManager];
    KKCryptoChartTimeType timeType = [manager getTimeTypeEnum:self.config.timeType];
    NSInteger secondTime = 0;//精确到秒
    if (timeType == KKCryptoChartTimeTypeOneMinute) {//1分钟
    	secondTime = 1*60;
    } else if (timeType == KKCryptoChartTimeTypeFiveMinutes) { //5分钟
    	secondTime = 5*60;
    } else if (timeType == KKCryptoChartTimeTypeFifteenMinutes) {//15分钟
    	secondTime = 15*60;
    } else if (timeType == KKCryptoChartTimeTypeThirtyMinutes) {//30分钟
    	secondTime = 30*60;
    } else if (timeType == KKCryptoChartTimeTypeOneHour) {//1小时
    	secondTime = 60*60;
    } else if (timeType == KKCryptoChartTimeTypeFourHours) {//4小时
		secondTime = 60*60;
    } else if (timeType == KKCryptoChartTimeTypeOneDay) {//1天
    	secondTime = 24*60*60;
    }
	
    NSInteger value = socketTimeStamp.integerValue - lastTimeStamp.integerValue;
    if (value > 0 && (value % secondTime == 0)) {
    	isAdd = YES;
    }
	
    return isAdd;
}

- (KKCryptoChartModel *)setupWebSocketModel:(NSDictionary *)klineData {
    KKCryptoChartModel *model = [[KKCryptoChartModel alloc] init];
    model.coinCode = [klineData valueForKey:@"symbol"];
    model.open = [klineData valueForKey:@"open"];
    model.close = [klineData valueForKey:@"close"];
    model.high = [klineData valueForKey:@"high"];
    model.low = [klineData valueForKey:@"low"];
	NSNumber *timeStamp = [klineData valueForKey:@"closeTime"];
	if (timeStamp.stringValue.length == 13) {//socket返回的时间戳为ms，https返回的为s，统一
		model.timestamp = @(timeStamp.integerValue/1000);
	} else {
		model.timestamp = [klineData valueForKey:@"closeTime"];
	}
    model.timeType = [[KKCryptoChartDataManager shareManager] getTimeStringFromSocketResolution:[klineData valueForKey:@"timeFrame"]];
    model.volume = [klineData valueForKey:@"volume"];
    model.change = [NSNumber numberWithInt:0];
    model.percentage = [klineData valueForKey:@"percentage"];
    return model;
}

#pragma mark - Tap Action

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint touchPoint = [touch locationInView:self];
    [self updateSelector:touchPoint];
    return YES;
}

- (void)updateSelector:(CGPoint)location {
    CGPoint moreLcation = [self.moreSelector.layer convertPoint:location fromLayer:self.layer];
    CGPoint tabBarLocation = [self.chartTabBar.layer convertPoint:location fromLayer:self.layer];
    CGPoint indicatorLocation = [self.indicatorSelector.layer convertPoint:location fromLayer:self.layer];

    // close selector

    if (!self.moreSelector.hidden) {
        if (![self.moreSelector.layer containsPoint:moreLcation] && ![self.chartTabBar.layer containsPoint:tabBarLocation]) {
            self.moreSelector.hidden = YES;
            [self.chartTabBar updateSelectorTabStatus];
        }
    }

    if (!self.indicatorSelector.hidden) {
        if (![self.indicatorSelector.layer containsPoint:indicatorLocation] && ![self.chartTabBar.layer containsPoint:tabBarLocation]) {
            self.indicatorSelector.hidden = YES;
            [self.chartTabBar updateSelectorTabStatus];
        }
    }
}

- (void)updateInfoLayer:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint mainChartLcation = [gestureRecognizer locationInView:self.mainChartView];
    if (![self.mainChartView.layer containsPoint:mainChartLcation]) {
        return;
    }
    //网格线间距
    if (mainChartLcation.x < self.frame.origin.x + 10 || mainChartLcation.x >= self.frame.size.width || mainChartLcation.y < 10 || mainChartLcation.y > self.mainChartView.frame.size.height) {
        return;
    }
    //点击像素点转换为图标数据
    CGPoint chartData = [self.mainChartView valueForTouchPointWithPoint:mainChartLcation axis:AxisDependencyRight];
    NSInteger selectIndex = (NSInteger)round(chartData.x);
//    ChartDataEntry *entry = [self.mainChartView getEntryByTouchPointWithPoint:mainChartLcation];
    if (self.chartModels.count <= selectIndex) {
        return;
    }
    KKCryptoChartModel *model = self.chartModels[selectIndex];
	self.currentChartModel = model;
    CGFloat pointY = (model.open.floatValue + model.close.floatValue)/2;//当前柱子的中心点
    CGFloat spec = self.mainChartView.frame.size.height/(self.visibleHighestPrice - self.visibleLowestPrice);//一刻度对应的height
    CGFloat cha = pointY - self.visibleLowestPrice;
    CGFloat hightLightY = self.mainChartView.frame.size.height - spec * cha;//柱子中心点对应的Frame
    self.highlight = [self.mainChartView getHighlightByTouchPoint:CGPointMake(mainChartLcation.x, hightLightY)];
    if (self.highlight != nil) {
        //绘制十字线及圆点
        [self.mainChartView highlightValue:self.highlight callDelegate:YES];
        self.highLightY = mainChartLcation.y;
        [self drawCrossLine:CGPointMake(self.highlight.xPx, mainChartLcation.y)];
        //绘制底部选中日期及价格
        [self drawInfoLayerText:CGPointMake(self.highlight.xPx, mainChartLcation.y) model:model];
        [self showCrossLine:YES];
    }

    [self.infoLayer updateAlertValueWithModel:model config:self.config];
    [self setupDescription:model config:self.config];
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress {
    switch (longPress.state) {
        case UIGestureRecognizerStateBegan:
            self.isLongPress = YES;
            break;
        case UIGestureRecognizerStateEnded:
            self.isLongPress = NO;
            break;
        default:
            break;
    }
    [self updateInfoLayer:longPress];
}

- (void)drawInfoLayerText:(CGPoint)location model:(KKCryptoChartModel *)model {
    [self drawVolumeText:location model:model];
    [self drawPriceText:location model:model];
}

- (void)drawVolumeText:(CGPoint)location model:(KKCryptoChartModel *)model {
    // 时间
    NSString *volumeString = [self getFormatterStringFromTimeType:model.timeType timestamp:model.timestamp];
    if (!volumeString) {
        return;
    }

    if (self.volumeLayer) {
        [self.volumeLayer removeFromSuperlayer];
        self.volumeLayer = nil;
    }
    CGSize volumeTextSize = [volumeString sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_XAXIS_TEXT_FONT }];
    CGRect volumeRect = CGRectMake(location.x - (volumeTextSize.width + AXIS_INFO_LAYER_X_OFFSET) / 2, CGRectGetMaxY(self.volumeChartView.frame) - KK_CRYPTO_CHART_XAXIS_TEXT_FONT.lineHeight, volumeTextSize.width + AXIS_INFO_LAYER_X_OFFSET * 2, KK_CRYPTO_CHART_XAXIS_TEXT_FONT.lineHeight);
    if (self.showChartViewArray.count > 0) {
        NSString *lastChartype = self.showChartViewArray.lastObject;
        CombinedChartView *combineView = [self chartViewFromCurrentTag:lastChartype.integerValue];
        volumeRect.origin.y = CGRectGetMaxY(combineView.frame) - KK_CRYPTO_CHART_XAXIS_TEXT_FONT.lineHeight;
    }
    self.volumeLayer = [[KKCryptoChartAxisInfoLayer alloc] init];
    self.volumeLayer.fontSize = 10;
    self.volumeLayer.alignmentMode = kCAAlignmentCenter;
    self.volumeLayer.xOffset = AXIS_INFO_LAYER_X_OFFSET;
    self.volumeLayer.text = volumeString;
    self.volumeLayer.frame = volumeRect;
    [self.layer insertSublayer:self.volumeLayer atIndex:10];
}

- (void)drawPriceText:(CGPoint)location model:(KKCryptoChartModel *)model {
    // 价格
    CGPoint chartData = [self.mainChartView valueForTouchPointWithPoint:location axis:AxisDependencyRight];
    NSNumber *currentY = [NSNumber numberWithDouble:chartData.y];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:self.config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:self.config.coinPrecision.integerValue];
    NSString *priceString = [formatter stringFromNumber:currentY];
    if (!priceString) {
        return;
    }
    if (self.priceLayer) {
        [self.priceLayer removeFromSuperlayer];
        self.priceLayer = nil;
    }
    CGSize priceTextSize = [priceString sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_YAXIS_TEXT_FONT }];
    CGRect priceRect = CGRectMake(CGRectGetMaxX(self.mainChartView.frame) - priceTextSize.width - AXIS_INFO_LAYER_X_OFFSET * 2 - 5.0, location.y + CHART_TABBAR_HEIGHT - KK_CRYPTO_CHART_YAXIS_TEXT_FONT.lineHeight / 2, priceTextSize.width + AXIS_INFO_LAYER_X_OFFSET * 2, KK_CRYPTO_CHART_YAXIS_TEXT_FONT.lineHeight);
    self.priceLayer = [[KKCryptoChartAxisInfoLayer alloc] init];
    self.priceLayer.fontSize = 8;
    self.priceLayer.alignmentMode = kCAAlignmentCenter;
    self.priceLayer.xOffset = AXIS_INFO_LAYER_X_OFFSET;
    self.priceLayer.text = priceString;
    self.priceLayer.frame = priceRect;
    [self.layer insertSublayer:self.priceLayer atIndex:10];
}

- (void)priceButtonClicked:(UIButton *)button {
    [self showCrossLine:NO];
    [self chartMoveToRight:self.chartModels isAnimated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.mainChartView.data notifyDataChanged];
        [self.mainChartView notifyDataSetChanged];

        [self.volumeChartView.data notifyDataChanged];
        [self.volumeChartView notifyDataSetChanged];

        for (int i = 0; i < self.showChartViewArray.count; i++) {
            NSString *currentIndicatorType = [self.showChartViewArray objectAtIndex:i];
            CombinedChartView *combineView = [self chartViewFromCurrentTag:currentIndicatorType.integerValue];
            if (combineView) {
                [combineView.data notifyDataChanged];
                [combineView notifyDataSetChanged];
            }
        }
        // refresh price line
        [self refreshPriceLine:self.chartModels.lastObject];
    });
}

//绘制十字交叉线
- (void)drawCrossLine:(CGPoint)point {
    
    if (self.loadingView.animating){
        return;
    }
    
    CGFloat y = point.y + CHART_TABBAR_HEIGHT;
    CGFloat x = point.x;
    
    //横线
    UIBezierPath *path_h = [[UIBezierPath alloc]init];
    [path_h moveToPoint:CGPointMake(0, y)];
    [path_h addLineToPoint:CGPointMake(self.mainChartView.frame.size.width, y)];
    self.horizontalLineLayer.path = path_h.CGPath;
    
    //竖线
    UIBezierPath *path_x = [[UIBezierPath alloc]init];
    [path_x moveToPoint:CGPointMake(x, self.chartTabBar.frame.size.height)];
    CGFloat lineHeight = self.frame.size.height - [[self.volumeChartView.xAxis getLongestLabel] sizeWithAttributes:@{ @"NSFontAttributeName" : KK_CRYPTO_CHART_XAXIS_TEXT_FONT }].height - 2.0f;
    [path_x addLineToPoint:CGPointMake(x, lineHeight)];
    self.verticalLineLayer.path = path_x.CGPath;

    //圆
    CGRect circleRect = CGRectMake(x-CROSS_SHAPED_CIRCLE_RADIUS/2, y-CROSS_SHAPED_CIRCLE_RADIUS/2, CROSS_SHAPED_CIRCLE_RADIUS, CROSS_SHAPED_CIRCLE_RADIUS);
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:circleRect cornerRadius:circleRect.size.width];
    self.circleLayer.path = circlePath.CGPath;
    [self.layer insertSublayer:self.circleLayer atIndex:10];
    
    //点
    CGRect pRect = CGRectMake(x-CROSS_SHAPED_CIRCLE_POINT_RADIUS/2, y-CROSS_SHAPED_CIRCLE_POINT_RADIUS/2, CROSS_SHAPED_CIRCLE_POINT_RADIUS, CROSS_SHAPED_CIRCLE_POINT_RADIUS);
    UIBezierPath *pPath = [UIBezierPath bezierPathWithRoundedRect:pRect cornerRadius:pRect.size.width];
    self.pointLayer.path = pPath.CGPath;
    [self.layer insertSublayer:self.pointLayer atIndex:10];
    [self.layer insertSublayer:self.infoLayer atIndex:10];
    [self changeInfoLayerFrame:x];
}

- (void)showCrossLine:(BOOL)isShow{
    if (isShow) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
    self.infoLayer.hidden = isShow == NO;
    
    self.pointLayer.hidden = isShow == NO;
    self.horizontalLineLayer.hidden = isShow == NO;
    self.verticalLineLayer.hidden = isShow == NO;
    self.circleLayer.hidden = isShow == NO;
    
    self.volumeLayer.hidden = isShow == NO;
    self.priceLayer.hidden = isShow == NO;
}

#pragma mark - lazy

- (CombinedChartView *)indicatorChartView
{
    if (!_indicatorChartView) {
        _indicatorChartView = [[CombinedChartView alloc] init];
        [self setUpIndicatorChartView:self.indicatorChartView];
        [self addSubview:self.indicatorChartView];
    }
    return _indicatorChartView;
}

- (CombinedChartData *)indicatorData
{
    if (!_indicatorData) {
        _indicatorData = [[CombinedChartData alloc] init];
    }
    return _indicatorData;
}

- (CombinedChartView *)kdjChartView
{
    if (!_kdjChartView) {
        _kdjChartView = [[CombinedChartView alloc] init];
        [self setUpIndicatorChartView:self.kdjChartView];
        [self addSubview:self.kdjChartView];
    }
    return _kdjChartView;
}

- (CombinedChartData *)kdjIndicatorData
{
    if (!_kdjIndicatorData) {
        _kdjIndicatorData = [[CombinedChartData alloc] init];
    }
    return _kdjIndicatorData;
}

- (CombinedChartView *)rsiChartView
{
    if (!_rsiChartView) {
        _rsiChartView = [[CombinedChartView alloc] init];
        [self setUpIndicatorChartView:self.rsiChartView];
        [self addSubview:self.rsiChartView];
    }
    return _rsiChartView;
}

- (CombinedChartData *)rsiIndicatorData
{
    if (!_rsiIndicatorData) {
        _rsiIndicatorData = [[CombinedChartData alloc] init];
    }
    return _rsiIndicatorData;
}

- (CombinedChartView *)wrChartView
{
    if (!_wrChartView) {
        _wrChartView = [[CombinedChartView alloc] init];
        [self setUpIndicatorChartView:self.wrChartView];
        [self addSubview:self.wrChartView];
    }
    return _wrChartView;
}

- (CombinedChartData *)wrIndicatorData
{
    if (!_wrIndicatorData) {
        _wrIndicatorData = [[CombinedChartData alloc] init];
    }
    return _wrIndicatorData;
}

- (KKIndicatorDescription *)mainIndicatorDesc
{
    if (!_mainIndicatorDesc) {
        _mainIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.mainChartView.frame.size.width - 10, 12)];
        _mainIndicatorDesc.hidden = YES;
        [self.mainChartView addSubview:self.mainIndicatorDesc];
    }
    return _mainIndicatorDesc;
}

- (KKIndicatorDescription *)volumeIndicatorDesc
{
    if (!_volumeIndicatorDesc) {
        _volumeIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.volumeChartView.frame.size.width - 10, 12)];
        [self.volumeChartView addSubview:self.volumeIndicatorDesc];
    }
    return _volumeIndicatorDesc;
}

- (KKIndicatorDescription *)sideIndicatorDesc
{
    if (!_sideIndicatorDesc) {
        _sideIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.mainChartView.frame.size.width - 10, 12)];
        [self.indicatorChartView addSubview:self.sideIndicatorDesc];
    }
    return _sideIndicatorDesc;
}

- (KKIndicatorDescription *)kdjIndicatorDesc
{
    if (!_kdjIndicatorDesc) {
        _kdjIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.mainChartView.frame.size.width - 10, 12)];
        [self.kdjChartView addSubview:self.kdjIndicatorDesc];
    }
    return _kdjIndicatorDesc;
}

- (KKIndicatorDescription *)rsiIndicatorDesc
{
    if (!_rsiIndicatorDesc) {
        _rsiIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.mainChartView.frame.size.width - 10, 12)];
        [self.rsiChartView addSubview:self.rsiIndicatorDesc];
    }
    return _rsiIndicatorDesc;
}

- (KKIndicatorDescription *)wrIndicatorDesc
{
    if (!_wrIndicatorDesc) {
        _wrIndicatorDesc = [[KKIndicatorDescription alloc] initWithFrame:CGRectMake(10, 5, self.mainChartView.frame.size.width - 10, 12)];
        [self.wrChartView addSubview:self.wrIndicatorDesc];
    }
    return _wrIndicatorDesc;
}

- (CAShapeLayer *)horizontalLineLayer {
    if (!_horizontalLineLayer){
        _horizontalLineLayer = [[CAShapeLayer alloc]init];
        _horizontalLineLayer.lineWidth = 0.5;
        [_horizontalLineLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2], nil]];
        [_horizontalLineLayer setStrokeColor:CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor];
        [self.layer insertSublayer:self.horizontalLineLayer atIndex:10];
    }
    return _horizontalLineLayer;
}

- (CAShapeLayer *)verticalLineLayer {
    if (!_verticalLineLayer){
        _verticalLineLayer = [[CAShapeLayer alloc]init];
        _verticalLineLayer.lineWidth = 0.5;
        _verticalLineLayer.hidden = YES;
        [_verticalLineLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInt:2], nil]];
        [_verticalLineLayer setStrokeColor:CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor];
        [self.layer insertSublayer:self.verticalLineLayer atIndex:10];
    }
    return _verticalLineLayer;
}

- (CAShapeLayer *)circleLayer {
    if (!_circleLayer){
        _circleLayer = [[CAShapeLayer alloc]init];
        _circleLayer.frame = CGRectMake(0, 0, CROSS_SHAPED_CIRCLE_RADIUS, CROSS_SHAPED_CIRCLE_RADIUS);
        _circleLayer.fillColor = CROSS_SHAPED_CIRCLE_COLOR.CGColor;
    }
    return _circleLayer;
}

- (CAShapeLayer *)pointLayer {
    if (!_pointLayer){
        _pointLayer = [[CAShapeLayer alloc]init];
        _pointLayer.frame = CGRectMake(0, 0, CROSS_SHAPED_CIRCLE_POINT_RADIUS, CROSS_SHAPED_CIRCLE_POINT_RADIUS);
        _pointLayer.fillColor = CROSS_SHAPED_CIRCLE_POINT_COLOR.CGColor;
    }
    return _pointLayer;
}

- (KKCryptoChartInfoLayer *)infoLayer {
    if (!_infoLayer){
        _infoLayer = [KKCryptoChartInfoLayer defaltInfoLayer];
    }
    return _infoLayer;
}

@end
