//
//  KKCryptoChartInfoLayer.swift
//  LineDemo
//
//  Created by slowdony on 2021/10/25.
//

import Foundation
import QuartzCore
import UIKit
//import OtherFile
@objc open class KKCryptoChartInfoLayer : CALayer {
    
    static let InfoLayer_Height = 180.0
    static let InfoLayer_Width = 136.0
    static let InfoLayer_ItemHeight = (KKCryptoChartInfoLayer.InfoLayer_Height-24.0)/8.0
    
    let redColor = UIColor(red: 255/255, green: 55/255, blue: 80/255, alpha: 1)
    let greenColor = UIColor(red: 30/255, green: 174/255, blue: 69/255, alpha: 1)
        
    //MARK: title
    //时间
    lazy var timeLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame: CGRect(x: 0.0, y: 12.0, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_time"))
        return layer
    }()
    
    //开
    lazy var openLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame: CGRect(x: 0.0, y: timeLayer.frame.height+timeLayer.frame.origin.y , width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_open"))
        return layer
    }()

    //关
    lazy var closeLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame:CGRect(x: 0.0, y: openLayer.frame.height+openLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_close"))
        return layer
    }()

    //高
    lazy var highLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame:CGRect(x: 0, y: closeLayer.frame.height+closeLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_high"))
        return layer
    }()

    //低
    lazy var lowLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame: CGRect(x: 0, y: highLayer.frame.height+highLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_low"))
        return layer
    }()

    //量
    lazy var numLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame:CGRect(x: 0, y: lowLayer.frame.height+lowLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_num"))
        return layer
    }()

    //涨跌幅
    lazy var changeLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame:CGRect(x: 0, y: numLayer.frame.height+numLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_change"))
        return layer
    }()

    //涨跌额
    lazy var changePercentageLayer: KKCryptoChartInfoItem = {
        let layer = KKCryptoChartInfoItem.init(frame: CGRect(x: 0, y: changeLayer.frame.height+changeLayer.frame.origin.y, width: KKCryptoChartInfoLayer.InfoLayer_Width, height: KKCryptoChartInfoLayer.InfoLayer_ItemHeight))
            layer.setItemTitle(title: KKCryptoChartDataManager.share().getLocalizableString(withKey: "chart_desc_change_percentage"))
        return layer
    }()
    
    //MARK: ----------MethodBegin----------
    override init() {
        super.init()
        self.addSublayer(self.timeLayer)
        self.addSublayer(self.openLayer)
        self.addSublayer(self.closeLayer)
        self.addSublayer(self.highLayer)
        self.addSublayer(self.lowLayer)
        self.addSublayer(self.numLayer)
        self.addSublayer(self.changeLayer)
        self.addSublayer(self.changePercentageLayer)
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    @objc open class func defaltInfoLayer() -> KKCryptoChartInfoLayer{
        
        let infoLayer = KKCryptoChartInfoLayer()
        infoLayer.frame = CGRect(x: 0, y: 0, width: KKCryptoChartInfoLayer.InfoLayer_Width, height:KKCryptoChartInfoLayer.InfoLayer_Height)
        // 修改提示信息弹窗
        infoLayer.backgroundColor = UIColor.init(red: 0.125, green: 0.125, blue: 0.125, alpha: 0.9).cgColor
        infoLayer.cornerRadius = 6
        infoLayer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        infoLayer.shadowOpacity = 1
        infoLayer.shadowRadius = 10
        infoLayer.shadowOffset = CGSize(width: 0, height: 2)
        return infoLayer
    }
     
     @objc open func updateAlertValue(model:KKCryptoChartModel?, config: KKCryptoChartGlobalConfig?){
         if model != nil
         {
             self.refreshDataWithChart(model: model!, config: config!)
         }
     }
     
    private func refreshDataWithChart(model:KKCryptoChartModel,config:KKCryptoChartGlobalConfig)
    {

         let formatter = NumberFormatter()
         formatter.maximumFractionDigits = Int(config.coinPrecision) ?? 0
         formatter.minimumFractionDigits = Int(config.coinPrecision) ?? 0

         //时间
         let titleValue = updateTimeFormat(timesTamp: model.timestamp.stringValue, config: config)
         self.timeLayer.setItemValue(value: titleValue)

         //开
         let openValue = formatter.string(from: model.open)
         self.openLayer.setItemValue(value: openValue ?? "")

         //收
         let closeValue = formatter.string(from: model.close)
         self.closeLayer.setItemValue(value: closeValue ?? "")

         //高
         let highValue = formatter.string(from: model.high)
         self.highLayer.setItemValue(value: highValue ?? "")

         //低
         let lowValue = formatter.string(from: model.low)
         self.lowLayer.setItemValue(value: lowValue ?? "")

         //量
         var numDoubleValue = model.volume.doubleValue
         var numValue = ""
         if numDoubleValue >= 1000{
             numDoubleValue = numDoubleValue/1000
             numValue = String(format: "%.2fk", numDoubleValue)
         }else{
             let tradeVolumePrecision = Int(config.tradeVolumePrecision) ?? 2
             numValue = String(format: "%.\(tradeVolumePrecision)f", numDoubleValue)
         }
         self.numLayer.setItemValue(value: numValue)


         //涨跌额 / 涨跌幅
         //涨跌额： 收-开
         //涨跌幅：（收-开）/开*100%；
         
        if (model.change == NSNumber.init(value: 0)){
            let changeDoubleValue = model.close.doubleValue - model.open.doubleValue
            let changePercentageValue = changeDoubleValue/model.open.doubleValue*100
            let changeNumber = NSNumber(value: changeDoubleValue)
            let changeValue = formatter.string(from: changeNumber)

            if changeDoubleValue > 0 {

                self.changeLayer.setItemValue(value: String(format: "+%@", changeValue ?? ""))
                self.changePercentageLayer.setItemValueTextColor(color: greenColor)
                self.changePercentageLayer.setItemValue(value: String(format: "+%.2f%%", changePercentageValue))
            }else{

                self.changeLayer.setItemValue(value: changeValue ?? "")
                self.changePercentageLayer.setItemValueTextColor(color: redColor)
                self.changePercentageLayer.setItemValue(value:String(format: "%.2f%%", changePercentageValue))
            }
        }else {
            let changeValue = model.change.doubleValue
            let changeStrValue = formatter.string(from: model.change)
            let percentageValue = model.percentage.doubleValue*100
            if changeValue > 0 {
                
                self.changeLayer.setItemValue(value: String(format: "+%@", changeStrValue ?? ""))
                self.changePercentageLayer.setItemValueTextColor(color: redColor)
                self.changePercentageLayer.setItemValue(value: String(format: "+%.2f%%", percentageValue))
            }else{
                self.changeLayer.setItemValue(value: changeStrValue ?? "")
                self.changePercentageLayer.setItemValueTextColor(color: greenColor)
                self.changePercentageLayer.setItemValue(value:String(format: "%.2f%%", percentageValue))
            }
        }
         
     }
    private func updateTimeFormat(timesTamp:String, config:KKCryptoChartGlobalConfig) -> String{
        var timeStr = ""
        let timeType = KKCryptoChartDataManager.share().getTimeTypeEnum(String(config.timeType))
        switch timeType {
        case .oneMinute:
            timeStr = DateUtil.getHourTime(fromTimestamp: timesTamp)
        case .fiveMinutes,.fifteenMinutes,.thirtyMinutes,.oneHour,.fourHours:
            timeStr = DateUtil.getTimeFromTimestamp(timesTamp)
        case .oneDay,.oneWeek,.oneMonth:
            timeStr = DateUtil.getMonthDay(fromTimestamp: timesTamp)
        case .oneYear:
            timeStr = DateUtil.getYearFromTimestamp(timesTamp)
        default:
            timeStr = ""
        }
       return timeStr
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class KKCryptoChartInfoItem:CALayer {
    
    ///Layer文字显示scale
    let  InfoContentScale : CGFloat = UIScreen.main.scale
    
    let  TextColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 0.9)
    
    lazy private var titleLayer: CATextLayer = {
        let layer = CATextLayer.init()
        layer.frame = CGRect(x: 15, y: 0 , width: 31.0, height: self.frame.height)
        layer.alignmentMode = CATextLayerAlignmentMode.left
        layer.contentsScale = InfoContentScale
        layer.foregroundColor = TextColor.cgColor
        layer.fontSize = 10.0
        layer.string = "title"
        return layer
    }()
    
    lazy private var valueLayer: CATextLayer = {
        let layer = CATextLayer.init()
        let x = titleLayer.frame.origin.x + titleLayer.bounds.width
        layer.frame = CGRect(x: x, y: 0, width:self.frame.width-15-x, height: self.frame.height)
        layer.alignmentMode = CATextLayerAlignmentMode.right
        layer.contentsScale = InfoContentScale
        layer.foregroundColor = TextColor.cgColor
        layer.fontSize = 10.0
        layer.string = "value"
        return layer
    }()
    
    init(frame:CGRect){
        super.init()
        self.frame = frame
        self.addSublayer(self.titleLayer)
        self.addSublayer(self.valueLayer)
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setItemTitle(title:String){
        self.titleLayer.string = title
        
        if title.isEmpty == false {
            let size = NSString(string: title).boundingRect(with: CGSize(width: CGFloat(MAXFLOAT), height: self.frame.height), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 10)], context: nil)
            if size.width>31{
                self.titleLayer.frame = CGRect(x: 15, y: 0, width: size.width, height: self.frame.height)
                let x = titleLayer.frame.origin.x + titleLayer.bounds.width
                self.valueLayer.frame = CGRect(x: x, y: 0, width:self.frame.width-15-x, height: self.frame.height)
            }
        }
    }
    
    func setItemValue(value:String){
        self.valueLayer.string = value
    }
    
    func setItemValueTextColor(color:UIColor){
        self.valueLayer.foregroundColor = color.cgColor
    }
}
 
