//
//  EncryptionUtil.h
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import <Foundation/Foundation.h>


@interface EncryptionUtil : NSObject

+ (NSString* )md5:(NSString* )input;
+ (NSString *)base64:(NSString *)input;

@end

