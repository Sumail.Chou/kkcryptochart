//
//  TDIndicatorValueFormatter.m
//  KKCryptoChart
//
//  Created by apple on 2021/6/25.
//

#import "TDIndicatorValueFormatter.h"

@implementation TDIndicatorValueFormatter

- (NSString *)stringForValue:(double)value entry:(ChartDataEntry *)entry dataSetIndex:(NSInteger)dataSetIndex viewPortHandler:(ChartViewPortHandler *)viewPortHandler {
    if ([entry isKindOfClass:[CandleChartDataEntry class]] && ((CandleChartDataEntry *)entry).data != nil && [((CandleChartDataEntry *)entry).data isKindOfClass:[KKTDModel class]]) {
        KKTDModel *td = ((CandleChartDataEntry *)entry).data;
        NSLog(@"TDIndicatorValueFormatter KKTDModel buySetupIndex:%ld, sellSetupIndex:%ld, buyCountdownIndex:%ld, sellCountdownIndex:%ld, TDSTBuy:%ld, TDSTSell:%ld", (long)td.buySetupIndex.integerValue, (long)td.sellSetupIndex.integerValue, (long)td.buyCountdownIndex.integerValue, (long)td.sellCountdownIndex.integerValue, (long)td.TDSTBuy.integerValue, (long)td.TDSTSell.integerValue);
        if (td.buySetupIndex.integerValue > 0) {
            return [NSString stringWithFormat:@"%ld", (long)td.buySetupIndex.integerValue];
        }
        if (td.sellSetupIndex.integerValue > 0) {
            return [NSString stringWithFormat:@"%ld", (long)td.sellSetupIndex.integerValue];
        }
    }
    return @"";
}

@end
