//
//  NetworkRequestUtil.m
//  KikiChartsDemo
//
//  Created by apple on 2021/2/22.
//

#import "NetworkRequestUtil.h"
#import "KKCryptoChartConstant.h"

@interface NetworkRequestUtil()

@end

@implementation NetworkRequestUtil

/*
 * fetch cloud data from specifical api
 */
+ (void)fetchCloudDataWithUrl:(NSString *)urlString completionHandler:(void (^)(NSDictionary * data, NSString * error))completionHandler {
    if (urlString == nil) {
        completionHandler(nil, @"fetchCloudDataWithUrl urlString is nil");
        return;
    }
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlString];
    KKLog(@"fetchCloudDataWithUrl url=%@", urlString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    request.HTTPMethod = @"GET";
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            KKLog(@"fetchCloudData error:%@", [error localizedDescription]);
            if (completionHandler) {
                completionHandler(nil, [error localizedDescription]);
            }
        } else {
            NSError *err = nil;
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            if (err || !dict) {
                KKLog(@"fetchCloudData Serialization failed");
                if (completionHandler) {
                    completionHandler(nil, [err localizedDescription]);
                }
            } else {
                id status = [dict objectForKey:@"success"];
                if (status == [NSNull null]) {
                    if (completionHandler) {
                        completionHandler(nil, @"response is not contain key:[success]");
                    }
                    return;
                }
                if (((NSNumber *)status).boolValue) {
                    id obj = [dict objectForKey:@"obj"];
                    if (obj == [NSNull null]) {
                        if (completionHandler) {
                            completionHandler(nil, @"response is not contain key:[obj]");
                        }
                        return;
                    }
                    if (completionHandler) {
                        completionHandler((NSDictionary *)obj, nil);
                    }
                } else {
                    if (completionHandler) {
                        completionHandler(nil, @"response status is not success");
                    }
                }
            }
        }
    }];
    [task resume];
}

@end
