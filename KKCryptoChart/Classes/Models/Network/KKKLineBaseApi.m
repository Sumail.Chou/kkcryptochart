//
//  KKKLineBaseApi.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKKLineBaseApi.h"

NSString *const BaseDevURL = @"http://api.dev.test-aspendigital.co/market/kline/init";
NSString *const BaseBetaURL = @"http://api.beta.test-aspendigital.co/market/kline/init";
NSString *const BaseProdURL = @"https://api.aspendigital.co/market/kline/init";

@interface KKKLineBaseApi()

@property (nonatomic, strong) NSString *baseUrl;

@end

@implementation KKKLineBaseApi

- (void)setEnvironment:(KKCryptoChartEnvironmentType)environment {
    _environment = environment;
    switch (environment) {
        case KKCryptoChartEnvironmentTypeDev:
            _baseUrl = BaseDevURL;
            break;
        case KKCryptoChartEnvironmentTypeBeta:
            _baseUrl = BaseBetaURL;
            break;
        case KKCryptoChartEnvironmentTypeProduct:
            _baseUrl = BaseProdURL;
            break;
        default:
            _baseUrl = BaseProdURL;
            break;
    }
}

- (NSString *)getBaseURL {
    return self.baseUrl;
}

- (NSString *)getCustomURL {
    return nil;
}

- (NSString *)getCompletedURL {
    return nil;
}

@end
