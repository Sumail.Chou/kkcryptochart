//
//  KKWebSocket.m
//  KKCryptoChart
//
//  Created by Henry on 2022/2/23.
//

#import "KKWebSocket.h"
#import "SRWebSocket.h"
#import "KKCryptoChartConstant.h"
#import "KKWebSocketRoom.h"
#import "KKWebSocketConfig.h"
#import "KKWebSocketMessage.h"
#import "EncryptionUtil.h"

NSString *const webSocketMsgPath = @"/api/room";
NSString *const webSocketMsgMethod = @"POST";
NSString *const webSocketMsgRoom = @"kline";
NSString *const webSocketHeadersAuthType = @"appCode";
NSString *const webSocketHeadersAppCode = @"c022929d0e1f4b0d8eace9cb9934bf0d";
NSString *const webSocketHeadersContentType = @"application/x-www-form-urlencoded; charset=utf-8";
NSString *const webSocketHeadersAcceptType = @"application/json; charset=utf-8";

@interface KKWebSocket()<SRWebSocketDelegate>

@property(nonatomic, strong) SRWebSocket *webSocket;
@property(nonatomic, assign) NSInteger randomValue;
@property(nonatomic, copy) NSString *urlString;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic, assign) NSInteger reconnectTime;

@end

@implementation KKWebSocket

- (instancetype)initWithURLString:(NSString *)urlString symbol:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution {
    if (self = [super init]) {
        _urlString = urlString;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
        _webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
        _webSocket.delegate = self;
        _resolution = resolution;
        _symbol = symbol;
        _fromSymbol = fromSymbol;
        _reconnectTime = 0;
    }
    return self;
}

// SRWebSockets are intended for one-time-use only. Open should be called once and only once.
// https://github.com/facebookincubator/SocketRocket/issues/480
- (void)reinitSocket {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    self.webSocket.delegate = self;
    self.reconnectTime = 0;
}

- (void)updateSocket:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution {
    _resolution = resolution;
    _symbol = symbol;
    _fromSymbol = symbol;
    _reconnectTime = 0;
    if (self.webSocket.readyState == SR_CLOSED || self.webSocket.readyState == SR_CLOSING) {
        [self reinitSocket];
        __strong typeof(self) wself = self;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC));
        dispatch_after(time, dispatch_get_main_queue(), ^{
            [wself sendMessage:wself.symbol fromSymbol:wself.fromSymbol resolution:wself.resolution];
        });
    } else {
        [self sendMessage:symbol fromSymbol:fromSymbol resolution:resolution];
    }
}

- (void)open {
    if (self.webSocket) {
        [self.webSocket open];
        __strong typeof(self) wself = self;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC));
        dispatch_after(time, dispatch_get_main_queue(), ^{
            [wself sendMessage:wself.symbol fromSymbol:wself.fromSymbol resolution:wself.resolution];
            [wself startTimer];
        });
    }
}

- (void)close {
    if (self.webSocket) {
        KKLog(@"webSocket close");
        [self.webSocket close];
        self.webSocket = nil;
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)startTimer {
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(sendBeatsOrRetryConnect) userInfo:nil repeats:YES];
    }
}

- (void)sendBeatsOrRetryConnect {
    if (!self.webSocket || !self.resolution) {
        return;
    }
    if (!self.isOpen) {
        [self close];
        self.reconnectTime = 0;
        [self reinitSocket];
        [self open];
    }
//    KKLog(@"webSocket sendBeatsOrRetryConnect readyState:%ld", (long)self.webSocket.readyState);
    if (self.webSocket.readyState == SR_OPEN) {
        self.reconnectTime = 0;
        [self.webSocket sendString:self.resolution error:nil];
    } else if (self.webSocket.readyState == SR_CLOSED || self.webSocket.readyState == SR_CONNECTING) {
        self.reconnectTime++;
        if (self.reconnectTime >= 10) {
            [self close];
            return;
        }
        [self reinitSocket];
        [self open];
    }
}

- (void)sendMessage:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution {
    KKWebSocketRoom *room = [[KKWebSocketRoom alloc] initWithRoom:webSocketMsgRoom symbol:symbol fromSymbol:fromSymbol resolution:resolution];
    KKWebSocketConfig *config = [[KKWebSocketConfig alloc] initWithAuthType:webSocketHeadersAuthType appCode:webSocketHeadersAppCode contentType:webSocketHeadersContentType acceptType:webSocketHeadersAcceptType];
    NSError *err;
    [self.webSocket sendString:[self generateMessage:room config:config urlString:self.urlString] error:&err];
}

- (NSString *)generateMessage:(KKWebSocketRoom *)room config:(KKWebSocketConfig *)config urlString:(NSString *)urlString {
    if (!room || !config || !urlString) {
        return nil;
    }
    KKWebSocketMessage *message = [[KKWebSocketMessage alloc] init];
    message.method = webSocketMsgMethod;
    message.path = webSocketMsgPath;
    NSURL * url = [NSURL URLWithString:urlString];
    message.host = [NSString stringWithFormat:@"%@:%@", url.host, url.port];

    // convert room object to string
    NSString *strRoom = [self generateMessageRoom:room];
    message.body = strRoom;
    message.headers = [self generateMessageHeaders:strRoom config:config];
    message.isBase64 = @0;

    // convert msg object to json
    NSDictionary *jsonMsg = [message dictionaryWithValuesForKeys:@[@"body", @"headers", @"host", @"isBase64", @"method", @"path"]];
    NSData *dataMsg = [NSJSONSerialization dataWithJSONObject:jsonMsg options:0 error:nil];
    NSString * strMsg = [[NSString alloc] initWithData:dataMsg encoding:NSUTF8StringEncoding];
    KKLog(@"generateMessage strMsg:%@", strMsg);
    return strMsg;
    // test string
//    NSString *strMsg = @"{\"body\":\"resolution\\u003dP15m\\u0026room\\u003dkline\\u0026symbol\\u003dBTC_USDT\",\"headers\":{\"content-type\":\"application/x-www-form-urlencoded; charset\\u003dutf-8\",\"accept\":\"application/json; charset\\u003dutf-8\",\"x-ca-deviceid\":\"1215846131\",\"Authorization\":\"APPCODE c022929d0e1f4b0d8eace9cb9934bf0d\",\"content-md5\":\"zV8raRk6iwn+SytYx21CVQ\\u003d\\u003d\\n\"},\"host\":\"161.117.178.192:18090\",\"isBase64\":0,\"method\":\"POST\",\"path\":\"/api/room\"}";
//    KKLog(@"generateMessage strMsg:%@", strMsg);
//    return strMsg;
}

- (NSString *)generateMessageRoom:(KKWebSocketRoom *)room {
    // convert room object to json
    NSDictionary *jsonRoom = [room dictionaryWithValuesForKeys:@[@"room", @"symbol", @"fromSymbol", @"resolution"]];
    NSMutableString *strRoom = [NSMutableString string];
    for (NSString *key in jsonRoom.allKeys) {
        if ([jsonRoom valueForKey:key]) {
            [strRoom appendString:[NSString stringWithFormat:@"%@=%@", key, [jsonRoom valueForKey:key]]];
            [strRoom appendString:@"&"];
        }
    }
    if (strRoom.length) {
        [strRoom deleteCharactersInRange:NSMakeRange(strRoom.length - 1, 1)];
    }
    return strRoom;
}

- (NSDictionary<NSString*, NSString*> *)generateMessageHeaders:(NSString *)body config:(KKWebSocketConfig *)config {
    if (!body || !config) {
        return nil;
    }
    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
    [mutDict setValue:config.contentType forKey:@"content-type"];
    [mutDict setValue:config.acceptType forKey:@"accept"];
    [mutDict setValue:@(self.randomValue) ?: @(arc4random()) forKey:@"x-ca-deviceid"];
    [mutDict setValue:[NSString stringWithFormat:@"APPCODE %@", config.appCode] forKey:@"Authorization"];
    NSString *md5 = [EncryptionUtil md5:body];
    [mutDict setValue:[EncryptionUtil base64:md5] forKey:@"content-md5"];
    return [[NSDictionary alloc] initWithDictionary:mutDict];
}

#pragma mark -- SRWebSocketDelegate --

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    self.isOpen = YES;
    [self sendRandomValue];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    KKLog(@"webSocket didFailWithError:%@", error.description);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    KKLog(@"webSocket didCloseWithCode:%ld, reason:%@", (long)code, reason);
    self.isOpen = NO;
}

- (void)sendRandomValue {
    if (!self.webSocket) {
        return;
    }
    self.randomValue = arc4random();
    NSError *err;
    [self.webSocket sendString:[NSString stringWithFormat:@"RG#%ld", (long)self.randomValue] error:&err];
    KKLog(@"sendRandomValue err:%@", [err description]);
}


- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(NSString *)string {
//    KKLog(@"webSocket didReceiveMessageWithString:%@", string);
    [self handleKlineData:string];
}

- (void)handleKlineData:(NSString *)klineString {
    if (!klineString || ![klineString hasPrefix:@"NF#"]) {
        return;
    }
    NSString *subString = [klineString substringFromIndex:3];
    NSData * _Nullable data = [subString dataUsingEncoding:NSUTF8StringEncoding];
    if (!data) {
        return;
    }
    NSError *err;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    if (![dict valueForKey:@"type"] || ![[dict valueForKey:@"type"] isEqualToString:webSocketMsgRoom]) {
        return;
    }
    if (![dict valueForKey:@"data"]) {
        return;
    }
    NSDictionary *klineDict = [dict valueForKey:@"data"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveKlineData:)]) {
        [self.delegate didReceiveKlineData:klineDict];
    }
}

@end
